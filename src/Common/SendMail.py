# -*- coding: utf-8 -*-
'''
Created on Sep 1, 2017
https://docs.python.org/2/library/email-examples.html#email-examples
@author: Lapbt
'''
import Common.SysSettings
from _datetime import datetime
from DataMining.HTMLProcessing import HTMLProcessing
#import socks
import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText

COMMASPACE = ', '

class SendMail(object):
    '''
    classdocs
    '''
    __mailserver = Common.SysSettings._mailserver   # "smtp.gmail.com"
    __mailserverport = Common.SysSettings._mailserverport # "587"
    __emailfrom = Common.SysSettings._mailsender
    __username = Common.SysSettings._mailusername
    __password = Common.SysSettings._mailpassword
    
    __emailto = None
    __fileToSend = None
    __subject = None
    __content = None
    
    
    def __init__(self, mailto, fileToSend='', subject='Hello', content='hello'):
        '''
        Constructor
        '''
        self.__emailto = mailto
        self.__fileToSend = fileToSend
        self.__subject = subject
        self.__content = content
        print ('%s Initiation sendmail successful' % str(datetime.now()))
        
        
    def __isHTML(self, sContent):
        oHTMLProcessing=HTMLProcessing()
        return oHTMLProcessing.isHTML(sContent)
        
        
    def Send(self):
        # Create message container - the correct MIME type is multipart/alternative here!    
        msg = MIMEMultipart()
        msg["From"] = self.__emailfrom
        #msg["To"] = self.__emailto
        msg["To"] = COMMASPACE.join(self.__emailto)        
        msg["Subject"] = self.__subject
        msg.preamble = self.__subject
        # 
        if self.__isHTML(self.__content):
            msg.attach(MIMEText(self.__content, 'html'))
        else:
            msg.attach(MIMEText(self.__content, 'plain'))
        
        if len(self.__fileToSend) > 0:
            ctype, encoding = mimetypes.guess_type(self.__fileToSend)
            if ctype is None or encoding is not None:
                ctype = "application/octet-stream"
            
            maintype, subtype = ctype.split("/", 1)
            
            if maintype == "text":
                fp = open(self.__fileToSend)
                # Note: we should handle calculating the charset
                attachment = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "image":
                fp = open(self.__fileToSend, "rb")
                attachment = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "audio":
                fp = open(self.__fileToSend, "rb")
                attachment = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(self.__fileToSend, "rb")
                attachment = MIMEBase(maintype, subtype)
                attachment.set_payload(fp.read())
                fp.close()
                encoders.encode_base64(attachment)
            attachment.add_header("Content-Disposition", "attachment", filename=self.__fileToSend)
            msg.attach(attachment)
        
        if Common.SysSettings._proxyYN == 'Y':
            #socks.setdefaultproxy(TYPE, ADDR, PORT)
            socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS4, Common.SysSettings._proxyIP, Common.SysSettings._proxyPort)
            socks.wrapmodule(smtplib)

        #server = smtplib.SMTP('%s:%s', (self.__mailserver, self.__mailserverport))
        #server = smtplib.SMTP('smtp.gmail.com', 587)
        server = smtplib.SMTP(self.__mailserver, self.__mailserverport)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(self.__username,self.__password)
        server.sendmail(self.__emailfrom, self.__emailto, msg.as_string())
        server.quit()
        
        return True
    
    
def testsendmail():
    mailto = ["Lapbt <buitienlap@gmail.com>"]
    fn = ''
    subject = """
        Your mail reader does not support the report format.
        Please visit us <a href="http://www.mysite.com">online</a>!"""
    
    email_content = """
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <title>html title</title>
          <style type="text/css" media="screen">
            table{
                background-color: #AAD373;
                empty-cells:hide;
            }
            td.cell{
                background-color: white;
            }
          </style>
        </head>
        <body>
          <!-- #######  YAY, I AM THE SOURCE EDITOR! #########-->
            <table width="700" cellspacing="0" cellpadding="10" align="center" bgcolor="#FFFFFF">
            <tbody>
            <tr>
            <td align="left">
            <p><strong>Dear Colleague,</strong></p>
            <p>We are pleased to present you our specialised newsletter including your academic job vacancies in schools of computer, electrical and mathematical sciences and engineering recently announced at AcademicGates.com&nbsp;worldwide.</p>
            <h2>Australia:</h2>
            <ul>
            <li><a href="http://www.engineeroxy.com/announcement,a3812.html" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://www.engineeroxy.com/announcement,a3812.html&amp;source=gmail&amp;ust=1509777430388000&amp;usg=AFQjCNFVQqgGcPKDTOtaaqKzvfq72CVKhw">Research Fellow in Robotics and Intelligent Systems</a>&nbsp;<span style="color: #ff6600;"><strong>PRIORITY!</strong></span><br />University of Sydney</li>
            <li><a href="http://www.computeroxy.com/announcement,a4898.html" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://www.computeroxy.com/announcement,a4898.html&amp;source=gmail&amp;ust=1509777430388000&amp;usg=AFQjCNFApWrycs9FFoHfDxI4fAjc5zz1eg">Lecturer in Pure Mathematics</a>&nbsp; <br />University of Melbourne, School of Mathematics and Statistics Faculty of Science</li>
            </ul>
            <h2>United State:</h2>
            <ul>
            <li><a href="http://www.engineeroxy.com/announcement,a3812.html" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://www.engineeroxy.com/announcement,a3812.html&amp;source=gmail&amp;ust=1509777430388000&amp;usg=AFQjCNFVQqgGcPKDTOtaaqKzvfq72CVKhw">Research Fellow in Robotics and Intelligent Systems</a>&nbsp;PRIORITY!<br />University of Sydney</li>
            <li><a href="http://www.computeroxy.com/announcement,a4832.html" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://www.computeroxy.com/announcement,a4832.html&amp;source=gmail&amp;ust=1509777430388000&amp;usg=AFQjCNG0tkRlXbdamKMvrI3fmJmqZHRHmQ">Computer Science (CS) Futures Fellow</a>&nbsp;PRIORITY!<br />ANU Australian National University</li>
            <li><a href="http://www.computeroxy.com/announcement,a4898.html" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://www.computeroxy.com/announcement,a4898.html&amp;source=gmail&amp;ust=1509777430388000&amp;usg=AFQjCNFApWrycs9FFoHfDxI4fAjc5zz1eg">Lecturer in Pure Mathematics</a>&nbsp;<br />University of Melbourne, School of Mathematics and Statistics Faculty of Science</li>
            <li></li>
            </ul>
            <p>&nbsp;</p>
            <p><span style="color: #000000; font-family: Tahoma; font-size: large;">and many more at&nbsp;<a href="http://academicgates.com/" target="_blank" rel="noopener">AcademicGates.com</a></span></p>
            <p>&nbsp;</p>
            <p>To learn more about these and other job vacancies, we invite you to visit our website&nbsp;<a href="http://academicgates.com/" target="_blank" rel="noopener">AcademicGates.com</a>.<br /><br />Want to post a job vacancy? Attract the attention of our academic audience of more than 630,000 professors, lecturers, researchers and academic managers who are at present employed in the highest-ranking&nbsp;schools of computer, electrical and mathematical sciences and engineering worldwide and who receive our specialised newsletters twice a month "<a href="http://computeroxy.com" target="_blank" rel="noopener">post a job vacancy on AcademicGates</a>".<br /><br />Yours faithfully</p>
            <p>Hung.Tran PhD</p>
            </td>
            </tr>
            <tr>
            <td align="center"><hr /><span style="color: #555555; font-family: tahoma; font-size: small;">ACADEMICGATES<br />Your Academic Vacancies in Schools of Computer, Electrical and Mathematical Sciences and Engineering<br />Europe - Asia - The Americas - Oceania - The Middle East - Africa<br /><a href="mailto:academia@academicgates.com" target="_blank" rel="noopener">academia@academicgates.com</a><br /><a href="http://www.academicgates.com/" target="_blank" rel="noopener">www.academicgates.com</a></span><hr /><span style="color: #555555; font-family: tahoma; font-size: small;">If you do not wish to receive AcademicGates newsletter or have received it in error, you may&nbsp;<a href="http://www.academicgates.com/deleteUserFromNewsletter,a73387945.html" target="_blank" rel="noopener"><span style="color: #555555;"><strong>unsubscribe</strong></span></a>. Thank you very much.</span></td>
            </tr>
            </tbody>
            </table>
        </body>
        """
    #email_content = "This is clear text"
    m = SendMail(mailto,fn, subject, email_content)
    m.Send()    
    
    
#testsendmail()   
    