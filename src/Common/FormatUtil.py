# -*- coding: utf-8 -*-
'''
Created on Aug 15, 2017

@author: Lapbt
'''
from dateutil.parser import parse
import datetime

def getDateFormat(txtText):
    '''
        Input: txtText: is a string in the format of date
        Output: 
            if txtText is a date get this one
            else
            return a default values datetime.datetime.now()+datetime.timedelta(days=45)
            with format yyyy/mm/dd
    '''
    txtText= txtText.replace("oktober","october")#
    if(isDate(txtText)):# if it is a instance of date time. Process further
        dt = parse(txtText)
        sDate= dt.strftime('%Y/%m/%d')
        
        if (str(sDate)> str(datetime.datetime.now()+datetime.timedelta(days=300))):
                    sDate= dt.strftime('%Y/%m/%d')
    else:
        dt=datetime.datetime.now()+datetime.timedelta(days=45)
        sDate= dt.strftime('%Y/%m/%d')   
         
    return sDate


def isDate(string):
    try: 
        parse(string)
        return True
    except ValueError:
        return False
        
        
def getDateTimeNow():
    currentDT = datetime.datetime.now()
    return str(currentDT)
    