# -*- coding: utf-8 -*-
'''
Created on Sep 1, 2017
https://docs.python.org/2/library/email-examples.html#email-examples
https://www.blog.pythonlibrary.org/2013/06/26/python-102-how-to-send-an-email-using-smtplib-email/
@author: Lapbt
'''
import Common.SysSettings
from _datetime import datetime
#from DataMining.HTMLProcessing import HTMLProcessing        # Edited by Lapbt. su dung truc tiep BeautifulSoup de kiem tra
from bs4 import BeautifulSoup
import socks
import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText
import email.utils as utils
import time
from DataAccess.MakeConnect import getConnection, getConnectionManual
from Common.FormatUtil import getDateTimeNow


COMMASPACE = ', '

class SendMultiMail(object):
    '''
    Send multi mail at one login
    '''
    __mailserver = None
    __mailserverport = 465
    __emailfrom = None
    __username = None
    __password = None
    
    # __emailto = None
    __fileToSend = None
    # __subject = None
    __content = None
    
    __msg = None
    __mail = None
    
    
    def __init__(self, mailsrvinfo):
        '''
        Constructor
        '''
        self.__mailserver = mailsrvinfo['host']
        self.__mailserverport = int(mailsrvinfo['port'])
        self.__emailfrom = mailsrvinfo['sender']
        self.__username = mailsrvinfo['user']
        self.__password = mailsrvinfo['pwd']
        self.__maildomain = str(self.__username.split('@')[1])
        
        # Create message container - the correct MIME type is multipart/alternative here!    
        self.__msg = MIMEMultipart()
        self.__msg["From"] = self.__emailfrom
        #self.__msg["To"] = COMMASPACE.join(self.__emailto)        
        #self.__msg["Subject"] = self.__subject
        #self.__msg.preamble = self.__subject
            
        print ('%s Initiation sendmail successful with sender [%s]' % (str(datetime.now()), self.__username))
        
    def __isHTML(self, sContent):
        return bool(BeautifulSoup(sContent, "html.parser").find())
        
    
    def login(self):
        if Common.SysSettings._proxyYN == 'Y':
            #socks.setdefaultproxy(TYPE, ADDR, PORT)
            socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS4, Common.SysSettings._proxyIP, Common.SysSettings._proxyPort)
            socks.wrapmodule(smtplib)

        # su dung giao thuc TLS port 587
        #self.__mail = smtplib.SMTP(self.__mailserver, self.__mailserverport)
        
        # giao thuc SSL port 465
        #Err: smtplib.SMTPServerDisconnected: Connection unexpectedly closed --> use smtplib.SMTP_SSL
        #self.__mail = smtplib.SMTP_SSL(self.__mailserver)
        #self.__mail.SMTP_SSL_PORT = self.__mailserverport
        
        if self.__mailserverport == 587:
            self.__mail = smtplib.SMTP()            
        else:
            self.__mail = smtplib.SMTP_SSL()
        
        # uncomment if interested in the actual smtp conversation
        if Common.SysSettings._maildebugYN == 'Y':
            self.__mail.set_debuglevel(1)
        
        self.__mail.connect(host=self.__mailserver, port=self.__mailserverport)
        #self.__mail.mail("noreply@academicgates.com", options="SMTPUTF8")
        self.__mail.mail(self.__username, options="SMTPUTF8")
        #self.__mail.local_hostname = "cloud194.hostgator.com"       # gia tri nay hien thi trong cho helo -> Received: from [1.52.126.10] (port=23907 helo=cloud194.hostgator.com)
        #self.__mail.local_hostname = "websitewelcome.com"
        #self.__mail.local_hostname = "academicgates.com"
        self.__mail.local_hostname = self.__mailserver
        #self.__mail.helo(name = "academicgates.com")
        
        
        try:
            if self.__mailserverport == 587:
                #self.__mail.ehlo()        # 11-sep-2021 tam dong 2 dong helo nay lai, vi che do tls k0 can
                self.__mail.starttls()
                #self.__mail.ehlo()
                print( 'Initiated TLS connection to SMTP server: %s' % self.__mailserver )
            else:
                self.__mail.ehlo()          # voi che do ssl, thi can helo
                print( 'Initiated SSL connection to SMTP server: %s' % self.__mailserver )
                            
        except RuntimeError as e:
            print( 'SSL/TLS support is not available to your Python interpreter: %s' % e )
        except smtplib.SMTPHeloError as e:
            print( "The server didn't reply properly to the HELO greeting: %s" % e )
            self.__mail.close()
            raise
        except smtplib.SMTPException as e:
            print( 'The server does not support the STARTTLS extension: %s' % e )
        
        if self.__username and self.__password:
            try:
                self.__mail.login( self.__username, self.__password )
                return True
            except smtplib.SMTPHeloError as e:
                print( "The server didn't reply properly to the HELO greeting: %s" % e )
                self.__mail.close()
                raise
            except smtplib.SMTPAuthenticationError as e:
                print( "The server didn't accept the username/password combination: %s" % e )
                self.__mail.close()
                raise
            except smtplib.SMTPException as e:
                print( "No suitable authentication method was found: %s" % e )
                self.__mail.close()
                raise
        
            
    def logout(self):
        if self.__mailserverport == 587:
            self.__mail.quit()            
        else:
            self.__mail.close()
        
            
    def Send(self, mailto, mailcc, mailbcc, subject='Hello', fileToSend='', content='hello'):        
        '''
        Inp:
            mailto: ["Hung can<tranhungemail@gmail.com>" , "Lapbt <buitienlap@gmail.com>"]
        Out:
            True | False
        '''
        
        self.__fileToSend = fileToSend
        self.__content = content
        
        
        if len(self.__fileToSend) > 0:
            ctype, encoding = mimetypes.guess_type(self.__fileToSend)
            if ctype is None or encoding is not None:
                #ctype = "application/octet-stream"
                ctype = "text/plain"
            
            maintype, subtype = ctype.split("/", 1)
            
            # lapbt sua tam cho nay de test loi mail o blackberry. Neu attach file text thi maintype = text luon
            if self.__fileToSend[-4:] == '.txt':
                maintype="text"
                
            maintype="text"
            
            if maintype == "application":
                fp = open(self.__fileToSend, "rb")
                attachment = MIMEBase(maintype, subtype)
                attachment.set_payload(fp.read())
                fp.close()
                encoders.encode_base64(attachment)                
            elif maintype == "image":
                fp = open(self.__fileToSend, "rb")
                attachment = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "audio":
                fp = open(self.__fileToSend, "rb")
                attachment = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(self.__fileToSend, "r", encoding="utf8")
                # Note: we should handle calculating the charset
                attachment = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
                
            attachment.add_header("Content-Disposition", "attachment", filename=self.__fileToSend)
            self.__msg.attach(attachment)
            
            
        if self.__isHTML(self.__content): 
            self.__msg.attach(MIMEText(self.__content, 'html'))
        else:
            self.__msg.attach(MIMEText(self.__content, 'plain'))
        
        mailreceiver = []
        if len(mailto)>0:
            self.__msg["To"] = COMMASPACE.join(mailto)
            mailreceiver = mailreceiver + mailto
        if len(mailcc)>0:
            self.__msg["Cc"] = COMMASPACE.join(mailcc)
            mailreceiver = mailreceiver + mailcc
        if len(mailbcc)>0:
            self.__msg["BCc"] = COMMASPACE.join(mailbcc)
            mailreceiver = mailreceiver + mailbcc
        
        print ('%s Sending' % str(datetime.now()))
        self.__msg["message-id"] = utils.make_msgid(domain=self.__maildomain)
        self.__msg["Subject"] = subject
        self.__msg.add_header('Content-Type', 'text/html')
        self.__msg.preamble = subject
        self.__mail.sendmail(self.__emailfrom, mailreceiver, self.__msg.as_string())
        self.__mail.rset()
        print ('%s Send success' % str(datetime.now()))
        
                
        return True
    
    
def sendTest():
    mailto = ["Lapbt <buitienlap@gmail.com>"]
    mailcc = [] #"Lapbt klb <lapbt.klb@gmail.com>"
    mailbcc = [] #"Lapbt live <buitienlap@live.com>"
    fn = ''
    subject = """
        Send test email
        """    
    email_content = """
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <title>Post Academic Jobs Free</title>          
        </head>
        <body>
            Hello     
        </body>
        """
    
    #email_content = "This is clear text"
    m = SendMultiMail(Common.SysSettings._mailSrvInf_aca_engine)
    #m = SendMultiMail(Common.SysSettings._mailSrvInf_gm_thunam)
    
    m.login()
    m.Send(mailto, mailcc, mailbcc, subject, fn, email_content)
    m.logout()
                    
                    
    
#sendTest()
