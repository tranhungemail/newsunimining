# -*- coding: utf-8 -*-
'''
Created on Aug 25, 2017

@author: Lapbt
'''

from selenium import webdriver
import os
import socket
import sys
import re
#import dns.resolver
import socket
import smtplib
import Common.SysSettings
from Common.SendMail import SendMail
from Common.SendMultiMail import SendMultiMail  # su dung mail cua ca
import pickle
from selenium.webdriver.chrome.options import Options 


if os.name != "nt":
    import fcntl
    import struct

    def get_interface_ip(ifname):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s',
                                ifname[:15]))[20:24])


def getHostName():
    return socket.gethostname()

def getIP():
    ip = socket.gethostbyname(socket.gethostname())
    if ip.startswith("127.") and os.name != "nt":
        interfaces = [
            "eth0",
            "eth1",
            "eth2",
            "wlan0",
            "wlan1",
            "wifi0",
            "ath0",
            "ath1",
            "ppp0",
            ]
        for ifname in interfaces:
            try:
                ip = get_interface_ip(ifname)
                break
            except IOError:
                pass
    return ip


class Log(object):
    def __init__(self, stoutLogFN):
        fn = getRootPath() + "\\" + stoutLogFN      
        self.orgstdout = sys.stdout
        self.log = open(fn, "a")

    def write(self, msg):
        self.orgstdout.write(msg)
        self.log.write(str(msg.encode('utf8'))) #UnicodeEncodeError: 'charmap' codec can't encode character '\uf028' in position 349: character maps to <undefined> --> https://stackoverflow.com/questions/27092833/unicodeencodeerror-charmap-codec-cant-encode-characters
    
    '''
    Fix error: Exception ignored in: <Common.Util.Log object at 0x000000458910F6A0>
                AttributeError: 'Log' object has no attribute 'flush'
    Read: https://stackoverflow.com/questions/20525587/python-logging-in-multiprocessing-attributeerror-logger-object-has-no-attrib
        Do khi doi huong tu console ra file. Sau do lai quay lai console thi phai them ham nay.
    ''' 
    def flush(self):
        pass 
        
def getChromeDriver():
    try:
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        #options.add_argument('--window-size=1920x1080')
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-infobars")
        options.add_argument("--disable-logging")
        options.add_argument("--disable-login-animations")
        options.add_argument("--disable-notifications")
        options.add_argument("--disable-default-apps")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-crash-reporter")
        options.add_argument("--disable-extensions")
        options.add_argument("--disable-in-process-stack-traces")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--log-level=2")   # Valid values are from 0 to 3: INFO = 0, WARNING = 1, LOG_ERROR = 2, LOG_FATAL = 3
        options.add_argument("--ignore-ssl-errors=yes")
        options.add_argument("--ignore-certificate-errors")
        options.add_experimental_option("excludeSwitches", ["enable-logging"]) # fix show error: Bluetooth: bluetooth_adapter_winrt.cc
                
        path=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Driver', 'chromedriver.exe')
        
        driver = webdriver.Chrome(chrome_options=options, executable_path=path)
        #driver = webdriver.Chrome( executable_path=path, service_log_path='NUL', )
        
        return driver
    except NameError:
            print ("Error at: getChromeDriver()")
            pass
    return False

def getMachineLearningModel():
    '''
       return f_Vector, LinearSVC_classifier
    ''' 
    #Load feature vector
    path=os.path.dirname(os.path.realpath(__file__))
    f_featuredVector=path+"\MachineLearningModel\FeaturedVector.pkl"
    with open(f_featuredVector, 'rb') as fid:
        f_Vector = pickle.load(fid)
    #Load Training Model
    f_classificationML=path+'\MachineLearningModel\MultipleLabelLinearSVC.pkl'
    with open(f_classificationML, 'rb') as fid:
        LinearSVC_classifier = pickle.load(fid)
        
    return (f_Vector,LinearSVC_classifier)



def getRootPath():
    dirname=os.path.dirname
    abspath = os.path.abspath
    return dirname(dirname(abspath(__file__)))

def sendExceptionErrorEmail(mailcontent, mailsubject):
    sMsg = '>>>>>>>>>>>> Error \n%s \n%s \n%s' % (str(sys.exc_info()[0]), str(sys.exc_info()[1]), str(sys.exc_info()[2]))
    mailcontent = mailcontent + '\n' + sMsg        
    print(sMsg)
    
    Common.Util.sendLogFileToUs("", "Exception Errors - %s" % mailsubject, mailcontent)
                
                
def sendLogFileToUs(logFN, subject, mailcontent):
    if len(logFN) > 0:
        fn = getRootPath() + "\\" + logFN
    else:    
        fn = ''       
    #print(fn)
    contentString = "The log file is sending from computer name: {} and IP {}".format(
        getHostName(), getIP()
    )
    mailcontent = contentString + '\n' + mailcontent 
    
    mailto = Common.SysSettings._mailto
    mailcc = []
    mailbcc = []
        
    #closed by lapbt 19/09/2020, k0 su dung gmail de gui log
    #m = SendMail(mailto,fn, subject, mailcontent)
    #m.Send()
    
    #bat co che do gui mail log (text)
    Common.SysSettings._mailenginelog = 'Y'
    
    #thay vao do se dung mail aca de gui, tuy nhien voi mail aca fai su dung class SendMultiMail moi dc (do class nay co chinh sua)
    senderinfo = Common.SysSettings._mailSrvInf_aca_engine
    m = SendMultiMail(senderinfo)
    m.login() 
    m.Send(mailto, mailcc, mailbcc, subject, fn, mailcontent)
    m.logout()
    
    
def clearLogFile(logFN):
    fn = getRootPath() + "\\" + logFN           
    #print(fn)
    try:
        os.remove(fn)
    except OSError:
        pass

def emailVerification(email, chkSMTP):
    '''
    https://www.scottbrady91.com/Email-Verification/Python-Email-Verification-Script
    https://stackoverflow.com/questions/22233848/how-to-verify-an-email-address-in-python-using-smtplib
    inp:
        email
    out:
        1: Success
        3: Invalid address
        4: DNS error
        5: SMTP error    
        -1: Undelivered     
    '''
    re_Success = 1
    re_InvalidAddress = 3
    re_DNSerror = 4
    re_SMTPerror = 5
    
    #Step 1: Check email
    #Check using Regex that an email meets minimum requirements, throw an error if not
    match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)
     

    if match == None:
        print('Invalid email address %s' % email)
        return re_InvalidAddress

    #Step 2: Getting MX record
    #Pull domain name from email address
    domain_name = email.split('@')[1]
    
    #get the MX record for the domain
    try:
        records = dns.resolver.query(domain_name, 'MX')
        mxRecord = records[0].exchange
        mxRecord = str(mxRecord)
    except dns.resolver.NXDOMAIN:
        print("Couldn't find any records (NXDOMAIN)")
        return re_DNSerror
    except dns.resolver.NoAnswer:
        print("The DNS response does not contain an answer (NoAnswer)")
        return re_DNSerror
    except dns.exception.Timeout:
        print('DNS query timeout')
        return re_DNSerror
    except dns.resolver.NoNameservers:
        print('answered SERVFAIL (NoNameservers)')
        return re_DNSerror
    
    if chkSMTP == True:
        #Step 3: ping email server
        #check if the email address exists
    
        # Get local server hostname
        #host = socket.gethostname()
        host = "academicgates.com"
            
        # SMTP lib setup (use debug level for full output)
        server = smtplib.SMTP()
        server.set_debuglevel(0)
        
        # SMTP Conversation
        try:
            server.connect(mxRecord)
            server.helo(host)
            server.mail('noreply@academicgates.com')
            code, message = server.rcpt(str(email))
            server.quit()
        except smtplib.socket.error:
            print('SMTP Connection timeout')
            return re_SMTPerror   
        
        # Assume 250 as Success
        if code == 250:
            print('Success')
            return re_Success
        else:
            print('Bad')
            return re_SMTPerror
    
    return re_Success
