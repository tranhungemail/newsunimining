# -*- coding: utf-8 -*-
'''
Created on Jan 8, 2021

@author: Lapbt
'''


from Common.WebsiteInfo import *
import Common.SysSettings
import Common.Util
#from DataAccess.MakeConnect import getConnection
from DataAccess.MakeConnect import getConnectionManual
from _datetime import datetime  # waring: fai de imp cu the o dong duoi import *
from contextlib import suppress
from prettytable import PrettyTable

class TaskManage(object):
    '''
    Mange all task of engine run by console
    '''
    __webinfo = None
    __oConn = None
    
    # Data default value
    __typCurrent = 'C'
    __typPrev = 'P'
    
    __stReady = 'ready'
    __stDoing = 'doing'
    __stDone = 'done'
    
    # Chi ap dung thoi gian dau tien
    __autoAddNewTask = True

    def __init__(self, website = None):
        '''
        Constructor
        '''
        self.__webinfo = website
        self.__oConn = getConnectionManual(website['dbserver'], website['dbname'], website['dbuser'], website['dbpwd'])
        
        print('%s Init TaskManage ok' % str(datetime.now()))
        
    
    def addTask(self, taskcode, taskname):
        '''
        Desc
        - Thêm mới taskcode
        - Chi thêm khi code chưa có trong danh sách
        '''
        sQuery = "select count(1) as c from log_enginetask " \
                 "where type = '%s' and code = '%s' " % (self.__typCurrent, taskcode)
        staResult = self.__oConn.selectSqlFetOne(sQuery)
        r = dict(staResult)['c']
        if r is None:
            r = 0
        else:
            r = int(r)
        
        if r==0:
            sIns = "INSERT INTO `log_enginetask`(`type`, `code`, `name`, `status`) " \
                    "VALUES('%s','%s','%s','%s') " % (self.__typCurrent, taskcode, taskname, self.__stReady)
            self.__oConn.executeNonQuery(sIns)
        
        return True
    
    
        
    def startTask(self, taskcode, taskname, taskcontent):
        '''
        Desc
        - Đánh dấu chạy vào task, bằng taskcode
        - Nếu taskcontent có nội dung, sẽ nối chuỗi ở trong db (áp dụng cho task lặp định kỳ như upload)
        '''
        
        #tu dong add them neu chua co
        if self.__autoAddNewTask:
            self.addTask(taskcode, taskname)
            
        sUp = "UPDATE log_enginetask " \
                "SET name = '%s', status = '%s', content = concat(content,' # ','%s'), starttime = now(), endtime = null " \
                "WHERE type = '%s' and code = '%s' " % (taskname, self.__stDoing, taskcontent, self.__typCurrent, taskcode)
        self.__oConn.executeNonQuery(sUp)
        
        return True
    
    
    def finishTask(self, taskcode):
        '''
        Desc
        - Đánh dấu ket thuc vào task, bằng taskcode
        
        '''
        sUp = "UPDATE log_enginetask " \
                "SET status = '%s', endtime = now() " \
                "WHERE type = '%s' and code = '%s' " % (self.__stDone, self.__typCurrent, taskcode)
        self.__oConn.executeNonQuery(sUp)
        
        return True
    
    
    def resetAllTask(self, save = True):
        '''
        Desc
        - Đánh dấu ket thuc vào task, bằng taskcode
        
        '''
        
        if save:
            sDel = "DELETE FROM log_enginetask " \
                "WHERE type = '%s' " % (self.__typPrev)
            self.__oConn.executeNonQuery(sDel)
            
            sIns = "INSERT INTO `log_enginetask`(`type`, `code`, `name`, `content`, `status`, `starttime`, `endtime`, `display_order`) " \
                "SELECT '%s', `code`, `name`, `content`, `status`, `starttime`, `endtime`, `display_order` FROM `log_enginetask` " \
                "WHERE type = '%s' " % (self.__typPrev, self.__typCurrent)
            self.__oConn.executeNonQuery(sIns)
                
        sUp = "UPDATE log_enginetask " \
                "SET status = '%s', starttime = null, endtime = null, content = '' " \
                "WHERE type = '%s' " % (self.__stReady, self.__typCurrent)
        self.__oConn.executeNonQuery(sUp)
        
        return True
        
        
        
    
    def __sendLog(self, logtype):
        '''
        send log by email
        
        '''
        #Common.SysSettings._mailsubject_AcaUploadNews
        mailsubject = 'Lotus engine log. Summary auto-run task'
        mailcontent = 'Task summary\n'
        
        sQuery = "select * from log_enginetask where type = '%s' order by display_order, code " % logtype   
        listLog = self.__oConn.selectSql(sQuery)
        
        t = PrettyTable(['Remark', 'No', 'Status', 'Task name', 'Start', 'End', 'Content'])
        t.align["Task name"] = "l"
        t.align["Content"] = "l"
        for row in listLog:
            highlighterr = ''
            if row['status'] != self.__stDone:
                highlighterr = '==>'
                
            t.add_row([highlighterr, row['display_order'], row['status'], '%s [%s]' % (row['name'],row['code']), row['starttime'], row['endtime'], row['content']])
        
        sdata = t.get_string(fields=["Remark", "Status", "Task name", "Content"])
        mailcontent = mailcontent + sdata
        print(mailcontent)
        
        #save to file
        logFN = 'stasklog.txt'
        fn = Common.Util.getRootPath() + "\\" + logFN
        with open(fn, 'wt') as f:
            f.write(t.get_string())
            
        Common.Util.sendLogFileToUs(logFN, Common.SysSettings._mailto, mailsubject, mailcontent)
        
        return True
    
    
    def sendCurrentLog(self):
        return self.__sendLog(self.__typCurrent)
    def sendPreviousLog(self):
        return self.__sendLog(self.__typPrev)
    
    
    
def test():
    telog = TaskManage(website = lJobhula)
    #telog.startTask(taskcode='D-Academic', taskname='Download from Academic site', taskcontent='')
    #tecode = 'test'
    #telog.startTask(taskcode=tecode, taskname='mining uk', taskcontent='uk')
    #telog.finishTask(taskcode=tecode)
    telog.sendCurrentLog()
    #telog.resetAllTask()

def test2():
    t = PrettyTable(['Name', 'Age'])
    t.add_row(['Alice', 24])
    t.add_row(['Bob', 19])
    a = 'test\n' + str(t)
    print(a)    
#test()
#test2()