'''
Created on Dec 7, 2017

@author: thg03
'''

from Common.FormatUtil import *
from Common.Util import *
from DataAccess import MakeConnect
from DataAccess.MakeConnect import getConnection
from DataAccess.MakeConnect import getConnectionManual
import re
import uuid
from contextlib import suppress
from _datetime import datetime

# imap
from itertools import chain
import imaplib
import email
import time
from Common.WebsiteInfo import lAcademia

regEmail = re.compile(("([a-z0-9!#$%&*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
                    "\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))



def ExtractEmailsFromText(txtValue, oConnection):
    ''' 
    This function will get all email address from text
        Input: txtValue, oConnection
        Output: An list: Email and UID
    '''    
    lstEmail=regEmail.findall(txtValue)
        
    
    if len (lstEmail)>0:
        for email in lstEmail:
            Email=email[0]
            UID=str(uuid.uuid4())
            if Common.SysSettings._verboselevel > 1:
                print("Checking: "+Email)
            with suppress(Exception):
                sql = "INSERT INTO tbl_professionalemail (`Email`, `UID`) VALUES (%s,%s)"                
                oConnection.executeNonQueryParaAslastrowid(sql, Email, UID)
                if Common.SysSettings._verboselevel > 1: 
                    print("Inserted email: "+ Email)    
        return True   
    
        
    return False

def ExtractEmailfromJob(txtValue):
    
    lstEmail=regEmail.findall(txtValue)
    if len (lstEmail)==0:
        return False
    
    lstEmailJob=[]
    for email in lstEmail:        
        lstEmailJob.append(email[0])
    
    return lstEmailJob

def EmailVerification():
    '''
    Load list of mail from a table -> verification -> update if it's not valid
    '''
    chkSMTP = False
    
    # get list of mail
    oConn = getConnectionManual(Common.SysSettings._dbHostName, 'mailmining', Common.SysSettings._dbUserName, Common.SysSettings._dbUserPwd)
    
    #sFullQuery = "select Email from tmp_professionalemail_281217 where active = 0"        
    sFullQuery = "select Email from tmp_clientemail_iran_30122017 where active = 3"
    lstOfMail = oConn.selectSql(sFullQuery)
    if lstOfMail:
        numberOfMail = len(lstOfMail)
    
    i = 0
    for row in lstOfMail:
        i += 1
        re = Common.Util.emailVerification(row['Email'], chkSMTP)
        print('%s Verify %s/%s: %s = %s' % (str(datetime.now()), i, numberOfMail, row['Email'], str(re)))        
        oConn.executeNonQueryAsRowcount("Update tmp_clientemail_iran_30122017 set Active = %s where Email = '%s'" % (str(re), row['Email']))
        

# ================================= Email Undelivered ============================
# def search_string(uid_max, criteria):
#     c = list(map(lambda t: (t[0], '"'+str(t[1])+'"'), criteria.items())) + [('UID', '%d:*' % (uid_max+1))]
#     return '(%s)' % ' '.join(chain(*c))
#     # Produce search string in IMAP format:
#     #   e.g. (FROM "me@gmail.com" SUBJECT "abcde" BODY "123456789" UID 9999:*)
# 
# def get_first_text_block(msg):
#     type = msg.get_content_maintype()
# 
#     if type == 'multipart':
#         for part in msg.get_payload():
#             if part.get_content_maintype() == 'text':
#                 return part.get_payload()
#     elif type == 'text':
#         return msg.get_payload()
#     
# 
# def get_recipients(msg_parsed):
#     """Given a parsed message, extract and return recipient list"""
#     # No user parameters below this line
#     ADDR_PATTERN = re.compile('<(.*?)>')  # Finds email as <nospam@nospam.com>
# 
#     recipients = []
#     addr_fields = ['From', 'To', 'Cc', 'Bcc']
# 
#     for f in addr_fields:
#         rfield = msg_parsed.get(f, "") # Empty string if field not present
#         rlist = re.findall(ADDR_PATTERN, rfield)
#         recipients.extend(rlist)
# 
#     return recipients
# 
#         
# def EmailUndelivered(webinfo, mailinfo):
#     '''
#     https://gist.github.com/nickoala/569a9d191d088d82a5ef5c03c0690a02
#     1. Conn to mailsrv at hosting. 
#     2. Download all undelivered email
#     3. Update to DB with Active = -1 (Undelivered)
#     
#     Inp
#         webinfo: infor to make connection
#         mailinfo: infor to connect to mailsrv (IMAP)
#     '''    
#     # get list of mail
#     #oConn = getConnectionManual(webinfo['dbserver'], webinfo['dbname'], webinfo['dbuser'], webinfo['dbpwd'])
#     
#     imap_ssl_host = mailinfo['host']  # imap.mail.yahoo.com
#     imap_ssl_port = 993                 # imap port
#     username = mailinfo['user']         # 'USERNAME or EMAIL ADDRESS'
#     password = mailinfo['pwd']         #'PASSWORD
#     
#     
#     # Restrict mail search. Be very specific.
#     # Machine should be very selective to receive messages.
#     criteria = {
#         'FROM':    '',
#         'SUBJECT': 'Undelivered Mail',
#         'BODY':    '',
#     }
#     uid_max = 0
#     
#     server = imaplib.IMAP4_SSL(imap_ssl_host, imap_ssl_port)
#     server.login(username, password)
#     server.select('INBOX')
#     
#     result, data = server.uid('search', None, search_string(uid_max, criteria))
#     
#     uids = [int(s) for s in data[0].split()]
#     #if uids:
#     #    uid_max = max(uids)
#         # Initialize `uid_max`. Any UID less than or equal to `uid_max` will be ignored subsequently.
#     
#     for uid in uids:
#         # Have to check again because Gmail sometimes does not obey UID criterion.
#         if uid > uid_max:
#             result, data = server.uid('fetch', str(uid), '(RFC822)')  # fetch entire message
#             #print(data[0][1])
#             msg = email.message_from_string(str(data[0][1]))
#             print(msg)            
#             uid_max = uid        
#             text = get_first_text_block(msg)
#             print('New message :::::::::::::::::::::')
#             print(text)
#             print(get_recipients(text))
# 
#     server.logout()
# 
# 
# EmailUndelivered(lAcademia, Common.SysSettings._mailSrvInf_aca_noreply)
# ============================================================
    
    
    
    
    
    
    
    
    
    
