# -*- coding: utf8 -*-

#import logging


'''
Created on Jul 28, 2017
Update 29 Apr 2018

This class is used to process HTML from joblisting

@author: thg03
'''
from bs4 import BeautifulSoup
import lxml.html.clean as clean
from langdetect import detect
from contextlib import suppress

from gensim.summarization import summarize  #pip install -U gensim
from lxml.html.clean import Cleaner


class HTMLProcessing():
             
    def cleanHTMLTagAttributes(self, txtHTML_dirty):
            '''
                Input: text of HTML with attributes
                Output: HTML without atrributes
            '''
            #safe_attrs = clean.defs.safe_attrs
            cleaner = clean.Cleaner(safe_attrs_only=True, safe_attrs=frozenset())
            txtHTML_cleansed = cleaner.clean_html(txtHTML_dirty)
            return txtHTML_cleansed
        
    
    def cleanHTMLandScript(self,oSoup):
            '''
                Input: a soup object
                Output: Strips all HTML tags and property and returns a plain text
            '''            
            for script in oSoup(["script", "style"]): # remove all javascript and stylesheet code
                script.extract()
            # get text
            text = oSoup.get_text()
            # break into lines and remove leading and trailing space on each
            lines = (line.strip() for line in text.splitlines())
            # break multi-headlines into a line each
            chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
            # drop blank lines
            text = '\n'.join(chunk for chunk in chunks if chunk)
            return text
    
    
    def cleanScript(self,txtHTML):
            '''
                Input: Text of HTML
                Output: return HTML without script and style
            ''' 
            soup =BeautifulSoup(txtHTML,"html.parser") # create a new bs4 object from the html data loaded
            for script in soup(["script", "style"]): # remove all javascript and stylesheet code
                script.extract()
            return str(soup)
        
    def cleanHTMLScript(self,txtHTML):
            '''
                Input: Text of HTML
                Output: Strips all HTML tags and property and returns a plain text
            ''' 
            soup =BeautifulSoup(txtHTML,"html.parser") # create a new bs4 object from the html data loaded
            for script in soup(["script", "style"]): # remove all javascript and stylesheet code
                script.extract()
            # get text
            #text = soup.get_text()
            # break into lines and remove leading and trailing space on each
            #lines = (line.strip() for line in text.splitlines())
            # break multi-headlines into a line each
            #chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
            # drop blank lines
            #text = '\n'.join(chunk for chunk in chunks if chunk)
            
            text=soup.text
            
            return text
        
    def isEnglish(self,txtTitle):
        '''
           Input: an text
           Output: True if it is English, False if it is not english
        '''
        lang=detect(txtTitle)
        if lang=="en":
            return True
        return False
            
        
    def isHTML(self,sContent):
        '''
           Input: string
           Output: True if it is HTML format, False if it is clear text string  
        '''
        soup = BeautifulSoup(sContent, 'lxml')
        links = soup.select('a')         # just check existed a href
        things_p = soup.find_all('p')      # find tag <p>
        if len(links) > 0 and len(things_p) > 0:
            return True
        return False
    
    def getJobAbstract(self,sHTML,ratio=0.20):
        '''
           Input: string of HTML
           Output: Abstract of Job
        '''
        txtAbstract=""
        with suppress(Exception):
            txtJob=self.cleanHTMLScript(sHTML)
            txtAbstract=summarize(txtJob,ratio)
            
        
        return txtAbstract
    


    def clean_HTML4News(self,dirty_html):
        
        cleaner = Cleaner(page_structure=True,
                      meta=True,
                      embedded=True,
                      links=True,
                      style=True,
                      processing_instructions=True,
                      inline_style=True,
                      scripts=True,
                      javascript=True,
                      comments=True,
                      frames=True,
                      forms=True,
                      annoying_tags=True,
                      remove_unknown_tags=True,
                      safe_attrs_only=True,
                      safe_attrs=frozenset(['src', 'href','data-src']),
                      remove_tags=('span', 'font', 'div')
                      )
    
        return cleaner.clean_html(dirty_html)
    