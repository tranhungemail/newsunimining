# -*- coding: utf-8 -*-
'''



@author: Hung Tran
'''

#%%
import RAKE
import re
from nltk.stem.wordnet import WordNetLemmatizer

from Common.FormatUtil import *
from Common.Util import *
from DataAccess import MakeConnect
from DataAccess.MakeConnect import getConnection
import re
import uuid
from contextlib import suppress
from nltk.tokenize import sent_tokenize, word_tokenize

from nltk.corpus import stopwords

import nltk
from gensim.summarization import keywords


import spacy
import pytextrank

def getKeywords_Gensim(text):
    lst_Keywords=keywords(text,lemmatize=True, split=True)
    return lst_Keywords


def getKeywords_RAKE(text="", VIP=[], min_score=1.5, show_score=False, reFormat=0):
    """
    keyword_extraction
    - Input:  
        + text (non html) 
        + VIP : list of VIP key words def: Null
        
        + min_score : min score to consider as key words def: 1.5
        + show_socre : show score in list def: False
        + reFormat=0: return a string of keywords each separeted by ";", else return a list of keywords.
        
    - Output: list of key words
    
    """
    # Clean text
    def text_cleaner(text="",email=False,link=False,html=False,rake=False,all=False,low=True):
        TAG_RE = re.compile(r'<[^>]+>')
   
        if email: text = re.sub(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+","", text) # remove email
        if link:  text = re.sub(r"http://+[a-z0-9\.\-+_/]+|https://+[a-z0-9\.\-+_/]+|www\.+[a-z0-9\.\-+_/]+","",text) #remove weblink
        if html: 
                #text = re.sub('<br>','\n',text) # keep newline
                text = TAG_RE.sub('.', text) # remove html tags
                text = re.sub('\.\.+','.', text) # remove ..
                text = re.sub("\s\s+" , " ", text) # remove spaces
        if rake:  
                text = re.sub(r'[^a-zA-Z()\'\"\.\,:;-]', ' ', text)
                text = re.sub("\s\s+" , " ", text) # remove spaces 
        if all: 
                text = re.sub(r'[^a-zA-Z]', ' ', text)
                text = re.sub("\s\s+" , " ", text) # remove spaces
        if low: text=text.lower()
        return(text)

    def text_split(text=""): # split text, keeping separators
        text_vec=[]
        v=""
        for c in text:
                if ord(c) in range(97,123): 
                        v=v+c
                else:
                        if v!="": 
                                text_vec.append(v)
                                v=""
                        text_vec.append(c)
        return(text_vec)
    
    # clean text
    text=text_cleaner(text,email=True,link=True,html=True,rake=True)
    #print(text)
    # convert to words
    text =text_split(text)
    #print(text)
    # Lemmatisation 
    lem = WordNetLemmatizer()
    text = [lem.lemmatize(word) for word in text] 
    text = "".join(text)
    #print(text)

    rake = RAKE.Rake(RAKE.SmartStopList())
    rake_keywords = rake.run(text, minCharacters = 3, maxWords = 3, minFrequency = 1)
    #print(rake_keywords)
    if rake_keywords!=[]: 
        inc=rake_keywords[0][1]/3
    else: inc=0
    #print(inc)
    # if key word is VIP, increase score
    new_rake=[]
    k=0
    for w in rake_keywords:
        new_rake.append(list(w))
        for i in VIP: 
            if i in w[0]: 
                new_rake[k][1]=rake_keywords[k][1]+inc 
        k+=1
    
    # sort the keywords
    def takeSecond(elem):
        return elem[1]
    new_rake.sort(key=takeSecond,reverse=True)

    # take the keyword with score >= min_score
    rake_keywords=[]
    if show_score:
        for w in new_rake:
            if w[1]>=min_score:
                rake_keywords.append(w)
    else:
        for w in new_rake:
            if w[1]>=min_score:
                rake_keywords.append(w[0])
    
    if (reFormat==0):
        tmpKeywords='; '.join(rake_keywords)
        sKeywords = (tmpKeywords[:tmpKeywords[:1000].rfind(";")] if len(tmpKeywords) > 1000 else tmpKeywords)
        return sKeywords
    
    return(rake_keywords)

def getKeywords(text):
    nlp = spacy.load("en_core_web_sm")
    
    # add PyTextRank to the spaCy pipeline
    tr = pytextrank.TextRank()
    nlp.add_pipe(tr.PipelineComponent, name="textrank", last=True)
    
    doc = nlp(text)
    tmpKeywords=str(doc._.phrases).replace("[","").replace("]","").replace(",",";")
    sKeywords = (tmpKeywords[:tmpKeywords[:1000].rfind(";")] if len(tmpKeywords) > 1000 else tmpKeywords)
    
    return sKeywords


#t1=getKeywords_New(text)
#print(t1)
#t2=getKeywords(text)
#print(t2)
#t3=getKeyword_TextRank(text)
#print(t3)



# example text

# load a spaCy model, depending on language, scale, etc.



