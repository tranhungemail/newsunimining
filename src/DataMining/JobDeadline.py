# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
Nguoi viet: Nhu + Hung
    - finding the deadline of jobs
    - Input: string - the content (without HTML) of job
    - Output: deadline of the job with form yyyy-mm-dd
"""

import datefinder
from date_extractor import extract_dates  
import re
import nltk
import datetime



def_number_of_available_days = 30 
def_length_characters_before_dates = 100 
def_switch_day_month = False            # If True, exchange day and month
def_deadline_limit_days = 360           # only take deadline date before and after today 360 days ago 

month_pattern = r"jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec"
date_format_num_MDY = r"(\d{1,2})(\D{1,2})(\d{1,2})(\D{1,2})(\d{4})"          # MM-DD-YYYY or DD-MM-YYYY  # output 5-component tuple
date_format_num_YMD = r'(\d{4})(\D{1,2})(\d{1,2})(\D{1,2})(\d{1,2})'           # YYYY - MM - DD
date_format_string_MDY =  r'(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)(\w{,6}.?\s)(\d{1,2})(\w{,2}.?\s)(\d{4})'
date_format_string_DMY = r'(\d{1,2})(\D{1,3})(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)(\w{,6}\D{1,2})(\d{4})' #'(\d{1,2})(.{,2}\s)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)(\w{,6}.?\s)(\d{4})'
date_format_string_YMD = r'(\d{4})(.)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)(\w{,6}\D{1,2})(\d{1,2})'
date_format_string_DM = r'(\d{1,2})(\D{1,3})(january|february|march|april|may|june|july|august|september|october|november|december)'   # only finding if above 5- formats cannot apply
date_format_string_MD = r'(january|february|march|april|may|june|july|august|september|october|november|december)(\D{1,3})(\d{1,2})'


date_format_in_text  = ['dd/mm/yyyy','dd.mm.yyyy','dd-mm-yyyy', 'dd mm yyyy']           # do NOT switch day and month

country_ID_switch = ['USA','CAN']
ID_Countries_encoding = ['DEU']

const_deadline_keywords = ['deadline', 'closing date', 'close date', 'final date', 'expire', 'last day', 'no later than',  'position until',
                           'application by', 'application until', 'application before','application via','application date', 'application close',
                           'applications close', 'application review date', 'applications received', 'applications will begin', 'end date',
                           'application will begin', 'application letter', 'application system', 'application received', 'applications must be', 
                           'application must be', 'date closing', 
                           'expiration', 'last date', 'closure date', 'expiry', 'fall due', 'date off', 'date-off', 'submit', 'apply before', 
                           'termination',  'due date', 'online by', 'send your app', 'as soon as possible or by', 'by email before','job closing',
                           'apply by', 'received by', 'close on','closes', 'filled or until', 'first date of consideration', 'pa pro rataclosing', 
                           'date of first consideration', 'first review date','be accepted until','be sent', 'paclosing','bewerbungsende','via email',
                           'via e-mail','through email','through e-mail','pdf file to','close at','sista ansökningsdag', 'considered until','applying online']

                    # 'submit before' 'submitted' 'should be sent' 'be sent by' 'application received by''review start date', 
                    # 'review begins on', 'review begin', 

class JobDeadline(object):
    # protect var
    _deadline_keywords = const_deadline_keywords
    
    # public vars    
    length_character_before_dates = def_length_characters_before_dates
        
    def __init__(self,num_of_available_day = def_number_of_available_days):
        self.num_of_available_day = num_of_available_day           
         
    def switch_day_month_func(self,text,date, country_ID, switch_day_month = def_switch_day_month):
        if (switch_day_month) or (country_ID not in country_ID_switch):
            if (re.search(month_pattern, text) != None):
                return date
            elif date.day < 13:
                return datetime.datetime(month = date.day, day = date.month, year = date.year)
            else:
                return date
        else:
            return date
            
    def extracting_text(self, full_text, last_index1, first_index2, last_index2):
        """
            Trich 1 doan text co do dai 32 ky tu trong full_text 
            - parameters: 
                + full_text: 1 doan text chua date
                + last_index1, last_index2: last_index1 < first_index2  < last_index2
                + len_of_date: do dai cua doan text can trich ra
        """
        min_len = min(first_index2 - last_index1,self.length_character_before_dates)
        return full_text[first_index2 - min_len:last_index2]
    
    def cleaning_text(self,txtJobContent, ID_Country):
        #txtJobContent = str(txtJobContent).encode('utf-8', 'ignore')
        #if ID_Country in ID_Countries_encoding:     
        try:        # encode('cp1252'): fix error: adc-sdd-sddff                                  
            txtJobContent = txtJobContent.encode('cp1252').decode("utf-8", "ignore")#.encode('cp65001').decode("utf-8", "ignore")
        except:     # encode('cp65001'): fix UnicodeEncodeError: 'charmap' codec can't encode character '\u0142'
            txtJobContent = txtJobContent.encode('cp65001').decode("utf-8", "ignore")
            #txtJobContent = txtJobContent.encode('cp65001').decode("utf-8", "ignore")  #.encode('latin-1')     .decode("utf-8", "ignore")
        tokens = nltk.word_tokenize(txtJobContent)         #decode('utf8').encode('latin1').decode('utf8')        
        words = [word.lower() for word in tokens if word != ' ']
        return ' '.join(words).replace(" ,", ",").replace(" /", "/").replace(" .", ".").replace(" -", "-")        
     
    def find_keyword_in_string(self, text):
        check_found_keyword = False
        for keyword in self._deadline_keywords:
            if text.find(keyword) != -1:
                #print('TEXT with keyword', text)
                #print('keyword', keyword)
                check_found_keyword = True
                break
        return check_found_keyword
    
    def finding_dates_re(self, txtJobContent, date_pattern):
        dates = re.findall(date_pattern, txtJobContent)
        #print('date re', dates)
        date_indices =[]
        if len(dates) >0:
            for date_tuple in dates:
                date_join = ''.join(date_tuple)
                date_index = txtJobContent.index(date_join)
                date_indices.append((date_index, date_index + len(date_join)))
            #print(date_indices)
            return dates, date_indices
        else:
            return None, None
    
    def finding_dates_without_year_re(self,txtJobContent,date_pattern):
        dates = re.findall(date_pattern, txtJobContent)
        #print('date re', dates)
        date_indices =[]
        if len(dates) >0:
            for date_tuple in dates:
                date_join = ''.join(date_tuple)
                date_index = txtJobContent.index(date_join)
                date_indices.append((date_index, date_index + len(date_join)))
            #print(date_indices)
            return dates, date_indices
        else:
            return None, None
    
    def deadline_without_year(self, nospace_job_content):
        deadline_set = []
        thisyear  = str( datetime.datetime.today().year)

        # String month-DD
        dates_string_MD, date_string_MD_indices = self.finding_dates_without_year_re(nospace_job_content, date_format_string_MD)
        #print(dates_string_MD)
        if dates_string_MD != None:
            date_string_MD_set = []
            date_string_MD_indices_new = []
            for j in range(len(dates_string_MD)):
                date_tuple = dates_string_MD[j]
                try:
                    date_created = datetime.datetime.strptime(date_tuple[2]+'-'+ date_tuple[0]+'-'+ thisyear,"%d-%B-%Y")
                    date_string_MD_set.append(date_created)
                    date_string_MD_indices_new.append(date_string_MD_indices[j])
                except:
                    pass
            for i in range(len(date_string_MD_set)):
                if i==0:
                    extracted_text = self.extracting_text(nospace_job_content, 0, date_string_MD_indices_new[i][0], date_string_MD_indices_new[i][1]) 
                else:
                    extracted_text = self.extracting_text(nospace_job_content, date_string_MD_indices_new[i-1][1], date_string_MD_indices_new[i][0], date_string_MD_indices_new[i][1]) 
                #print('extracted text', extracted_text)
                if self.find_keyword_in_string(extracted_text):
                    deadline_set.append(date_string_MD_set[i])
        
        # String D-month
        dates_string_DM, date_string_DM_indices = self.finding_dates_re(nospace_job_content, date_format_string_DM)   
        #print(dates_string_DM)
        if dates_string_DM != None:
            date_string_DM_set = []
            date_string_DM_indices_new = []
            for j in range(len(dates_string_DM)):
                date_tuple = dates_string_DM[j]
                try:
                    date_created = datetime.datetime.strptime(date_tuple[0]+'-'+date_tuple[2]+'-'+thisyear,"%d-%B-%Y")    
                    date_string_DM_set.append(date_created)
                    date_string_DM_indices_new.append(date_string_DM_indices[j])
                except:
                    pass
            for i in range(len(date_string_DM_set)):
                if i==0:
                    extracted_text = self.extracting_text(nospace_job_content, 0, date_string_DM_indices_new[i][0], date_string_DM_indices_new[i][1]) 
                else:
                    extracted_text = self.extracting_text(nospace_job_content, date_string_DM_indices_new[i-1][1], date_string_DM_indices_new[i][0], date_string_DM_indices_new[i][1]) 
                # print('extracted text', extracted_text)
                if self.find_keyword_in_string(extracted_text):
                    deadline_set.append(date_string_DM_set[i])
        
        ### get minimum deadline
        if len(deadline_set) > 0:
            #print('deadline candidates', deadline_set)
            return min(dt for dt in deadline_set if dt!= None).strftime("%Y-%m-%d")


    def switch_day_and_month(self,txtJobContent):
        # Return: True if DD-MM-YYYY
        # dates var is a list of date with 5-components (0-month, 2- day, 4-year)
        switch_daymonth = False
        for date_format in date_format_in_text:
            if txtJobContent.find(date_format) !=-1:
                switch_daymonth = True
                break
            
        if (not switch_daymonth): 
            dates = self.finding_dates_re(txtJobContent, date_format_num_MDY)[0]
            if dates != None:
                #day_max = max([int(date[2]) for date in dates])
                month_max = max([int(date[0]) for date in dates])
                if (month_max > 12):
                    switch_daymonth = True
        return switch_daymonth
                
        
    def creating_date_from_day_month(self,country_ID, day, month, year,switch_day_month = def_switch_day_month):
        if (max(day,month) <32):
            if (switch_day_month) or (country_ID not in country_ID_switch):
                if day < 13:
                    try:
                        return datetime.datetime(year = year, month = day, day = month)
                    except: 
                        pass
                else:
                    return datetime.datetime(year = year, month = month, day = day)
            else:
                if month < 13:
                    try: 
                        return datetime.datetime(year = year, month = month, day = day)
                    except: 
                        pass    
    
    def deadline_finder_re(self, nospace_job_content, country_ID, switch_day_month = def_switch_day_month):
        # finding dates with num format: MM-DD-YYYY or DD-MM-YYYY
        dates_MDY, date_MDY_indices = self.finding_dates_re(nospace_job_content, date_format_num_MDY)
        deadline_set = []
        if dates_MDY != None:
            date_MDY_set = []
            date_MDY_indices_new = []
            for j in range(len(dates_MDY)):
                date_tuple = dates_MDY[j]
                try:
                    date_created = self.creating_date_from_day_month(country_ID, int(date_tuple[2]), int(date_tuple[0]), int(date_tuple[-1]), switch_day_month)
                    date_MDY_set.append(date_created)
                    date_MDY_indices_new.append(date_MDY_indices[j])
                except:
                    pass
            for i in range(len(date_MDY_set)):
                if i==0:
                    extracted_text = self.extracting_text(nospace_job_content, 0, date_MDY_indices_new[i][0], date_MDY_indices_new[i][1]) 
                else:
                    extracted_text = self.extracting_text(nospace_job_content, date_MDY_indices_new[i-1][1], date_MDY_indices_new[i][0], date_MDY_indices_new[i][1]) 
                #print('extracted text', extracted_text)
                if self.find_keyword_in_string(extracted_text):
                    deadline_set.append(date_MDY_set[i])
        # YYYY-MM-DD
        dates_YMD, date_YMD_indices = self.finding_dates_re(nospace_job_content, date_format_num_YMD)
        if dates_YMD != None:
            date_YMD_set = []
            date_YMD_indices_new = []
            for j in range(len(dates_YMD)):
                date_tuple = dates_YMD[j]
                try:
                    date_created = datetime.datetime(day = int(date_tuple[-1]), month = int(date_tuple[2]), year = int(date_tuple[0]))
                    date_YMD_set.append(date_created)
                    date_YMD_indices_new.append(date_YMD_indices[j])
                except:
                    pass
            for i in range(len(date_YMD_set)):
                if i==0:
                    extracted_text = self.extracting_text(nospace_job_content, 0, date_YMD_indices_new[i][0], date_YMD_indices_new[i][1]) 
                else:
                    extracted_text = self.extracting_text(nospace_job_content, date_YMD_indices_new[i-1][1], date_YMD_indices_new[i][0], date_YMD_indices_new[i][1]) 
                #print('extracted text', extracted_text)
                if self.find_keyword_in_string(extracted_text):
                    deadline_set.append(date_YMD_set[i])

        # String M-DD-YYY
        dates_string_MDY, date_string_MDY_indices = self.finding_dates_re(nospace_job_content, date_format_string_MDY)
        if dates_string_MDY != None:
            date_string_MDY_set = []
            date_string_MDY_indices_new = []
            for j in range(len(dates_string_MDY)):
                date_tuple = dates_string_MDY[j]
                try:
                    date_created = datetime.datetime.strptime(date_tuple[2]+'-'+ date_tuple[0]+'-'+ date_tuple[-1],"%d-%b-%Y")
                    date_string_MDY_set.append(date_created)
                    date_string_MDY_indices_new.append(date_string_MDY_indices[j])
                except:
                    pass
            for i in range(len(date_string_MDY_set)):
                if i==0:
                    extracted_text = self.extracting_text(nospace_job_content, 0, date_string_MDY_indices_new[i][0], date_string_MDY_indices_new[i][1]) 
                else:
                    extracted_text = self.extracting_text(nospace_job_content, date_string_MDY_indices_new[i-1][1], date_string_MDY_indices_new[i][0], date_string_MDY_indices_new[i][1]) 
                #print('extracted text', extracted_text)
                if self.find_keyword_in_string(extracted_text):
                    deadline_set.append(date_string_MDY_set[i])

        # String D-M-Y
        dates_string_DMY, date_string_DMY_indices = self.finding_dates_re(nospace_job_content, date_format_string_DMY)   
        if dates_string_DMY != None:
            date_string_DMY_set = []
            date_string_DMY_indices_new = []
            for j in range(len(dates_string_DMY)):
                date_tuple = dates_string_DMY[j]
                try:
                    date_created = datetime.datetime.strptime(date_tuple[0]+'-'+date_tuple[2]+'-'+date_tuple[-1],"%d-%b-%Y")    
                    date_string_DMY_set.append(date_created)
                    date_string_DMY_indices_new.append(date_string_DMY_indices[j])
                except:
                    pass
            for i in range(len(date_string_DMY_set)):
                if i==0:
                    extracted_text = self.extracting_text(nospace_job_content, 0, date_string_DMY_indices_new[i][0], date_string_DMY_indices_new[i][1]) 
                else:
                    extracted_text = self.extracting_text(nospace_job_content, date_string_DMY_indices_new[i-1][1], date_string_DMY_indices_new[i][0], date_string_DMY_indices_new[i][1]) 
                #print('extracted text', extracted_text)
                if self.find_keyword_in_string(extracted_text):
                    deadline_set.append(date_string_DMY_set[i])
        # String YMD
        dates_string_YMD, date_string_YMD_indices = self.finding_dates_re(nospace_job_content, date_format_string_YMD)
        if dates_string_YMD != None:
            dates_string_YMD_set = []
            date_string_YMD_indices_new = []
            for j in range(len(dates_string_YMD)):
                date_tuple = dates_string_YMD[j]
                #print(date_tuple)
                try:
                    date_created = datetime.datetime.strptime(date_tuple[-1]+'-'+date_tuple[2]+'-'+date_tuple[0],"%d-%b-%Y")    
                    dates_string_YMD_set.append(date_created)
                    date_string_YMD_indices_new.append(date_string_YMD_indices[j])
                except:
                    pass
            for i in range(len(dates_string_YMD_set)):
                if i==0:
                    extracted_text = self.extracting_text(nospace_job_content, 0, date_string_YMD_indices_new[i][0], date_string_YMD_indices_new[i][1]) 
                else:
                    extracted_text = self.extracting_text(nospace_job_content, date_string_YMD_indices_new[i-1][1], date_string_YMD_indices_new[i][0], date_string_YMD_indices_new[i][1]) 
                #print('extracted text', extracted_text)
                if self.find_keyword_in_string(extracted_text):
                    deadline_set.append(dates_string_YMD_set[i])
                   
        if len(deadline_set) > 0:
            # print('test with year')
            return min(dt for dt in deadline_set if dt!= None).strftime("%Y-%m-%d")
        else:
            # Finding dealine format (without YYYY): DD-month or month-DD
            # print('test without year')
            return self.deadline_without_year(nospace_job_content)
             
        
                            
    def deadline_finder(self, nospace_job_content, country_ID, switch_day_month = def_switch_day_month):
        """
            Return: date of deadline of jobs
            Paramaters:
                - job_content: string type
                - switch_month_day: TRUE => Switch the day and month 
        """        
        min_deadline = (datetime.datetime.today() - datetime.timedelta(days = def_deadline_limit_days)).strftime("%Y-%m-%d")
        max_deadline = (datetime.datetime.today() + datetime.timedelta(days = def_deadline_limit_days)).strftime("%Y-%m-%d")

        datefinder.ValueError = ValueError, OverflowError  ### To deal with the error below
        """To deal with the error:  OverflowError: Python int too large to convert to C long"""
        date_matchs = datefinder.find_dates(nospace_job_content, index=True)
        extractor_matchs = extract_dates(nospace_job_content)          # using date_extractor and datefinder to filter
        extractor_dates = [date2.strftime("%Y-%m-%d") for date2 in extractor_matchs if (date2 != None)]
        
        # print('extrator dates', extractor_dates)
        if len(extractor_dates) >0:
            date_indices = []
            dates = []
            for date, index in date_matchs:
                date_formatted = date.strftime("%Y-%m-%d")
                    # print('date', date)
                if (date_formatted >= min_deadline) and (date_formatted <= max_deadline) and (date_formatted in extractor_dates):
                    dates.append(date)
                    date_indices.append(index)

            if len(date_indices) >0:
                deadline_set = []
                for i in range(len(date_indices)):
                    if i==0:
                        extracted_text = self.extracting_text(nospace_job_content, 0, date_indices[i][0], date_indices[i][1]) 
                    else:
                        extracted_text = self.extracting_text(nospace_job_content, date_indices[i-1][1], date_indices[i][0], date_indices[i][1]) 
                    #print('extracted text', extracted_text)
                    if self.find_keyword_in_string(extracted_text):
                        switched_date = self.switch_day_month_func(extracted_text, dates[i],country_ID, switch_day_month)
                        deadline_set.append(switched_date)
                         
                if len(deadline_set) > 0:
                    #print('found date set', deadline_set)
                    return min(dt for dt in deadline_set if dt!= None).strftime("%Y-%m-%d") 

        
    def getJobDeadline_Main(self, txtJobContent, country_ID, switch_day_month = def_switch_day_month):
        """
            Return: date of deadline of jobs
            Paramaters:
                - job_content: string type
                - switch_month_day: TRUE => Switch the day and month 
        """        
        #print(txtJobContent)
        nospace_job_content = self.cleaning_text(txtJobContent, country_ID)
               
        # defining no_switch_day_month
        switch_day_month_defined = self.switch_day_and_month(nospace_job_content)
        if switch_day_month_defined:
            switch_day_month = switch_day_month_defined
        #print('MM-DD-YYYY FORMAT', switch_day_month_defined)
        
        deadline_re = self.deadline_finder_re(nospace_job_content, country_ID, switch_day_month)
        if deadline_re != None:
            return deadline_re
        else:
            #print('library-implementation')
            deadline_found_by_libraries = self.deadline_finder(nospace_job_content, country_ID, switch_day_month)
            if deadline_found_by_libraries != None:
                return deadline_found_by_libraries
            else:
                #print('default deadline')
                return (datetime.datetime.today() + datetime.timedelta(days = self.num_of_available_day)).strftime("%Y-%m-%d")
                
                
        
    
    
