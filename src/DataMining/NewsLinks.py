'''
Created on May 6, 2019

@author: thg03

'''

import datetime
import uuid
from DataMining.MachineLearning import TextProcessing
from DataMining.ExtractEmails import ExtractEmailsFromText
from DataMining.ExtractKeyWords import getKeywords
from DataMining.HTMLProcessing import HTMLProcessing
from datetime import timedelta

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import re
from langdetect import detect
from Common.FormatUtil import getDateFormat

import csv



import Common.SysSettings
from Common.Util import getMachineLearningModel


class NewsLink():
    __oCnn = None
    __backDate=None
    
    def __init__(self, oConn):
        self.__oCnn=oConn
        d = datetime.datetime.today() - timedelta(days=500)#
        self.__backDate=d.strftime('%Y-%m-%d')
        
    def Load_OldNewsLinks(self, ID_University="",iStatus=-1):
        '''
          Purpose of this method is to return a table that content all old news links.
          Input: ID_University
          Output: tbl_NewsLinks in one year in which all jobs link have been scanned data. 
        '''
        try:
            #sql="SELECT ProcessingDate, ID_University FROM tbl_University WHERE ID_University=%s"
            #aDateofPost = self.__oCnn.selectSqlFetOnePara(sql, ID_University)
            #print(len(aDateofPost))
            
            #sql="SELECT OriginalLink FROM tbl_Positions WHERE ID_University=%s and DateofPost=%s"  
            #table = self.__oCnn.selectSqlPara(sql, ID_University, aDateofPost['ProcessingDate'])
            #sql="SELECT OriginalLink FROM tbl_Positions WHERE ID_University=%s"
            
             
            sDate= self.__backDate
            
            if iStatus==-1:
                sql="SELECT OriginalLink FROM tbl_NewsLinks WHERE ID_University=%s and CollectedDate >=%s ORDER BY CollectedDate DESC"
                
                tbl_NewsLinks = self.__oCnn.selectSqlPara(sql, ID_University, sDate)
            else:
                sql="SELECT OriginalLink FROM tbl_NewsLinks WHERE ID_University=%s and CollectedDate >=%s and Status=%s ORDER BY CollectedDate DESC"
                
                tbl_NewsLinks = self.__oCnn.selectSqlPara(sql, ID_University, sDate, iStatus)
            
            
            Old_NewsLinks=[]
            for row in tbl_NewsLinks:
                Old_NewsLinks.append(row["OriginalLink"])
                
            
            return Old_NewsLinks
        except NameError:
            print (">>>>>>>> Error at NewsLink Class, method Load_OldNewsLinks()")            
            #Save to log files            
            pass
        return None
    
    
    
    
    
    def insert_One_NewsLink(self,ID_University, ID_Country, ID_NewsLink, Title, OriginalLink, Status=0, Reason=2):     
        '''
            Input: oConnection, and list of parameters for tbl_OldJobLinks
            Output: true if insert successfully otherwise false.
        '''
        try:
            sql = "INSERT INTO tbl_NewsLinks(ID_University, ID_Country, ID_NewsLink, Title, OriginalLink,Status, CollectedDate,Reason) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
            
            CollectedDate=str(datetime.datetime.now().date()).replace("-","/")
            lastrowid = self.__oCnn.executeNonQueryParaAslastrowid(sql,ID_University, ID_Country, ID_NewsLink, Title, OriginalLink, Status, CollectedDate,Reason)
            
            if Common.SysSettings._verboselevel > 1:
                print(' - %s insert_One_NewsLink lastrowid: %s' % (str(datetime.datetime.now()), lastrowid))            
                print(" - ID News Link:"+ID_NewsLink)
                print(" - Title:"+Title)
            return lastrowid
        except NameError:
            print (">>>>>>>> Error at NewsProcessing Class, method insert_One_NewsLink()")            
            #Save to log files            
            pass   
        return False
    
    def update_One_NewsLink(self,ID_University, ID_Index, Status):        
        '''
            Input: oConnection, and list of parameters for tbl_OldNewsLinks
            Output: true if insert successfully otherwise false.
        '''
        try:
            sql = "UPDATE tbl_NewsLinks SET Status=%s, ProcessedDate=%s WHERE ID_University=%s and ID_Index=%s"
            
            sUpdateDateTime=getDateFormat(str(datetime.datetime.now()))
            self.__oCnn.executeNonQueryParaAslastrowid(sql,Status, sUpdateDateTime, ID_University, ID_Index)
            
            if Common.SysSettings._verboselevel > 1:
                print('%s Update_One_NewsLink at ID_Index: %s, Status=%s' % (str(datetime.datetime.now()), ID_Index,str(Status) ))            
                #print("ID_Index:"+str(ID_Index)+" - Status:"+str(Status))
            return True
        except NameError:
            print (">>>>>>>> Error at NewsProcessing Class, method update_One_NewsLink()")            
            #Save to log files            
            pass   
        return False
   
        
        #lstOriginalLink2=[row['OriginalLink'] for row in table]
    
    
    def returnNewNewsLinks(self,tbl_WebsiteNewsLinks, tbl_NewsLinks):
        
        
        lstNewNewsLinks_from_Website=[]
        NumberofOldLink=len(tbl_NewsLinks)
        
        
        for itm in tbl_WebsiteNewsLinks:  
            
            if(len(itm)==3):# Khong co ID Job
                sWeb=str(itm[2]).strip()
            else:# Co ID Job
                sWeb=str(itm[3]).strip()
            
            isFound=False
            j=0
            
            while (j<NumberofOldLink) and (isFound==False):
                sDB=str(tbl_NewsLinks[j])
                    
                if (sDB.find(sWeb)!=-1):
                    isFound=True
                else:
                    j=j+1
                
            if isFound==False:
                lstNewNewsLinks_from_Website.append(itm)  
        #print(lstNewJobLinks_from_Website)
        return lstNewNewsLinks_from_Website
    """        
        if(isIDJob==False):                
            #Scan data from web and compare with DBSet 
            lstNewJobLinks_from_Website=[]
       
                sWeb=str(tbl_WebsiteJobLinks[i][2]).strip()                    
                isFound=False
                j=0
                while (j<NumberofOldLink) and (isFound==False):
                    sDB=str(sList_Of_OriginalLinklstOriginalLink[j]).strip()
                    if (sDB.find(sWeb)>0):
                        isFound=True
                    else:
                        j=j+1
                    
                    if isFound==False:
                        lstNewJobLinks_from_Website.append(tbl_WebsiteJobLinks[i])            
            else:
                #Scan data from web and compare with DBSet        
                for i in range(len(tbl_WebsiteJobLinks)):
                    
                    sWeb=str(tbl_WebsiteJobLinks[i][3]).strip()
                    isFound=False
                    j=0
                    while (j<NumberofOldLink) and (isFound==False):
                        sDB=str(sList_Of_OriginalLinklstOriginalLink[j]).strip()
                        if (sDB.find(sWeb)>0):
                            isFound=True
                        else:
                            j=j+1
                    
                    if isFound==False:
                        lstNewJobLinks_from_Website.append(tbl_WebsiteJobLinks[i])    
    """
        
    def getNewNewsLinks(self, ID_University, tbl_WebsiteNewsLinks):
        '''
            Input:  oConnection, ID_University and news links in the website of university.
            Output: a table contains New News Links which need to be inserted into the database.
        '''
        
        try:
            
            
            sList_Of_OriginalLinklstOriginalLink=self.Load_OldNewsLinks(ID_University) 
            lstNewNewsLinks_from_Website=self.returnNewNewsLinks(tbl_WebsiteNewsLinks, sList_Of_OriginalLinklstOriginalLink)
                
                    
            
                    
            return lstNewNewsLinks_from_Website
        except NameError:
            print (">>>>>>>> Error at NewsLinks Class, method getNewNewsLinks()")            
            #Save to log files            
            pass     
    
    
    def isJobLinkExist(self,aJobLink,ID_University):
        '''
            Input:  aJobLink, oConnection, ID_University and job links in the website of university.
            Output: number of job link
        '''
        try:
            lstOriginalLink=self.mergeNewsLinkLists(ID_University)
            found=False
            if(aJobLink in lstOriginalLink):
                found=True            
            return found
        except NameError:
            print (">>>>>>>> Error at JobLinks Class, method isJobLinkExist()")            
            #Save to log files            
            pass  
           
        