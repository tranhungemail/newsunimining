# -*- coding: utf-8 -*-
'''
Created on Feb 8, 2018

@author: Lotus

Description:
- Status = 0: Invalid; 1: Published; 2: Approved; 4: Ready; 5: Pending; 7: Downloaded; 8: New; 9: Draft
- Scan rec_userpostjobs with status in (8)
- Processing: nonhtml, keywords, researcharea
- Finish:
    + All infor ok --> Set Status = 4 (ready to approve)
    + Else         --> Set Status = 5 (pending, need to update info)
'''
import sys
sys.path.append('C:\Lotus\Engine_Python\src')

from difflib import SequenceMatcher         # neu sau dung nhieu, co the dua vao Common, tam thoi de day
from Common.WebsiteInfo import *
import Common.SysSettings
import Common.FormatUtil
from DataAccess.MakeConnect import getConnectionManual
from _datetime import datetime  # waring: fai de imp cu the o dong duoi import *
from DataMining.JobProcessing import JobProcessing
from DataMining.HTMLProcessing import HTMLProcessing
from DataMining.ExtractKeyWords import getKeywords
from DataMining.MachineLearning import TextProcessing
from Common.FormatUtil import getDateFormat



def GenStatisticUserPost(oConn = None):
    # vars
    sta_Reject = 0
    sta_Publish = 1
    sta_Accept = 2
    sta_Ready = 4
    sta_Pending = 5
    sta_New = 8
    sta_Draft = 9	
    
    sMsg = '\n\nIII. Summary: generate user post statistic'
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
    
    # user org
    sMsg = '\nUser Organization'
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
    
    sQuery = "SELECT `status`, COUNT(1) as cnt FROM `tbl_su_user_org` WHERE Source = 'M' GROUP BY `status`"
    listStatistic = oConn.selectSql(sQuery)
    for row in listStatistic:
        if row['status'] == sta_Reject:
            name = 'Reject'
        elif row['status'] == 1:
            name = 'Active'
        elif row['status'] == 2:
            name = 'Accept'        
        elif row['status'] == 9:
            name = 'Under Review =========>'
        else:
            name = 'Undefine'
        
        if (row['status'] == 9) and row['cnt']>0:
            Common.SysSettings._processuserpostsendlogYN = 'Y'   # co new org can review la se gui mail
        
        sMsg = '%s [%s] = %s' % (name, row['status'], str(row['cnt']))
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
        print(sMsg)
    
    # user post
    sMsg = '\n\nUser post'
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
    
    sQuery = "SELECT `status`, COUNT(1) as cnt FROM `rec_userpostjobs` WHERE `Status` <> -1 and deleted_at is NULL GROUP BY `Status`"
    listStatistic = oConn.selectSql(sQuery)
    for row in listStatistic:
        if row['status'] == sta_Reject:
            name = 'Reject'
        elif row['status'] == sta_Publish:
            name = 'Publish'
        elif row['status'] == sta_Accept:
            name = 'Accept'
        elif row['status'] == sta_Ready:
            name = 'Under Review (Ready) =============>'
        elif row['status'] == sta_Pending:
            name = 'Under Review (Pending) =============>'
        elif row['status'] == sta_New:
            name = 'New'
        elif row['status'] == sta_Draft:
            name = 'Draft'			
        else:
            name = 'Undefine'
        
        if (row['status'] == sta_Ready or row['status'] == sta_Pending or row['status'] == sta_New) and row['cnt']>0:
            Common.SysSettings._processuserpostsendlogYN = 'Y'   # co new post can review la se gui mail
        
        sMsg = '%s [%s] = %s' % (name, row['status'], str(row['cnt']))
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
        print(sMsg)
        
    
    if (Common.SysSettings._processuserpostsendlogYN == 'Y'):
        # New user post by month
        sMsg = '\nUser post by month'
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
        print(sMsg)
      
        sQuery = "SELECT MONTH(DateofPost) as m, CustomerID, Status, COUNT(1) as c FROM `rec_userpostjobs` " \
                    "WHERE (MONTH(DateofPost) = MONTH(UTC_DATE()) OR MONTH(DateofPost) = MONTH(UTC_DATE() - INTERVAL 1 MONTH)) " \
                    "GROUP BY MONTH(DateofPost), CustomerID, Status " \
                    "HAVING COUNT(1) > 2 " \
                    "ORDER BY MONTH(DateofPost), CustomerID, Status " \
                    ""
        staResult = oConn.selectSql(sQuery)
        for row in staResult:
            sMsg = '\nMonth [%s]; CustomerID [%s]; Status [%s]; Jobs [%s]' % (str(row['m']), row['CustomerID'], str(row['Status']), str(row['c']))
            Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
            print(sMsg)
            
        # Top user post job
        sMsg = '\n\nTop User post job, all time'
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
        print(sMsg)
       
        sQuery = "SELECT CustomerID, COUNT(1) as c FROM `rec_userpostjobs` " \
                    "WHERE Status=1 " \
                    "GROUP CustomerID " \
                    "HAVING COUNT(1) > 2 " \
                    "ORDER BY COUNT(1) DESC, CustomerID " \
                    "LIMIT 0,20 " \
                    ""
        staResult = oConn.selectSql(sQuery)
        for row in staResult:
            sMsg = '\nCustomerID [%s]; Jobs [%s]' % (row['CustomerID'], str(row['c']))
            Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
            print(sMsg)
        
        # Top user post by month
        sMsg = '\nTop User post by month'
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
        print(sMsg)
      
        sQuery = "SELECT MONTH(DateofPost) as m, CustomerID, COUNT(1) as c FROM `rec_userpostjobs` " \
                    "WHERE Status = 1 AND (MONTH(DateofPost) = MONTH(UTC_DATE()) OR MONTH(DateofPost) = MONTH(UTC_DATE() - INTERVAL 1 MONTH)) " \
                    "GROUP BY CustomerID, MONTH(DateofPost) " \
                    "HAVING COUNT(1) > 2 " \
                    "ORDER BY MONTH(DateofPost) DESC, COUNT(1) DESC, CustomerID " \
                    "LIMIT 0,20 " \
                    ""
        staResult = oConn.selectSql(sQuery)
        for row in staResult:
            sMsg = '\nMonth [%s]; CustomerID [%s]; Jobs [%s]' % (str(row['m']), row['CustomerID'], str(row['c']))
            Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
            print(sMsg)
        


            
        # All project
        sMsg = '\n\All recruitment project'
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
        print(sMsg)
        
        sQuery = "SELECT COUNT(1) as cntProject, SUM(CountCandidates) as cntCandi FROM `rec_projects` WHERE `Status` = 1"
        staResult = oConn.selectSqlFetOne(sQuery)
        cntProject = dict(staResult)['cntProject']
        cntCandi = dict(staResult)['cntCandi']
        
        sMsg = '\nAll Project = [%s]; All Candidate = [%s]' % (str(cntProject), str(cntCandi))
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
        print(sMsg)
        
        
        # New project by month
        sMsg = '\Recruitment project by month'
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
        print(sMsg)
        
        sQuery = "SELECT MONTH(CreatedDate) as m, CustomerID, Status, COUNT(1) as c, SUM(CountCandidates) as cntCandi FROM `rec_projects` " \
                    "WHERE (MONTH(CreatedDate) = MONTH(UTC_DATE()) OR MONTH(CreatedDate) = MONTH(UTC_DATE() - INTERVAL 1 MONTH)) " \
                    "GROUP BY MONTH(CreatedDate), CustomerID, Status " \
                    "HAVING COUNT(1) > 1 " \
                    "ORDER BY MONTH(CreatedDate), CustomerID, Status " \
                    ""
        staResult = oConn.selectSql(sQuery)
        for row in staResult:
            sMsg = '\nMonth [%s]; CustomerID [%s]; Status [%s]; Project [%s]; Candidate [%s]' % (str(row['m']), row['CustomerID'], str(row['Status']), str(row['c']), str(row['cntCandi']))
            Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
            print(sMsg)
            
           

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def ProcessOrgOfUser(oConn = None):
    # vars
    sta_Active = 1
    sta_New = 9
    
    # get list of new user-org
    sQuery = "select ID_University, customerid, UniversityName, UniversityLink from tbl_su_user_org where Source = 'M' and (IsSimilarUniList is NULL or IsSimilarUniList = '') and Status = %s" % sta_New
    listOfOrg = oConn.selectSql(sQuery)
    numberOfOrg = len(listOfOrg) 
    sMsg = '%s Processing new user org: %s <========' % (str(datetime.now()), str(numberOfOrg))
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
    if numberOfOrg == 0:
        return
    
    # get list of active university
    sQuery = "select ID_University, UniversityName, UniversityLink from tbl_university where Status = %s" % sta_Active
    listOfActive = oConn.selectSql(sQuery)
    
    i=0
    for row in listOfOrg:
        i += 1
        sMsg = '%s/%s: %s' % (i, numberOfOrg, row['UniversityName'])
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
        print(sMsg.encode('utf8'))
        
        compOrgUni = []
        for uni in listOfActive:
            ratio_Name = similar(row['UniversityName'], uni['UniversityName'])  
            ratio_Link = similar(row['UniversityLink'], uni['UniversityLink'])
            
            if ratio_Name > 0.5 and ratio_Link > 0.6:
                compOrgUni.append([uni['ID_University'], ratio_Name + ratio_Link, uni['UniversityName']])
            
        # check result
        compOrgUni.sort(key=lambda x:x[1], reverse=True)
        if len(compOrgUni) == 0:
            strSimilar = 'NONE'
            strSimilarMail = 'NONE'
        else:
            maxItems = 5        # chi lay 5 gia tri cao nhat
            strSimilar = ''
            strSimilarMail = ''
            j = 0
            for compitem in compOrgUni:
                j=j+1
                strSimilar = strSimilar + '%s:%s;' % (compitem[0], compitem[1])
                strSimilarMail = strSimilarMail + '%s:%s;\n' % (compitem[2], compitem[1])
                if j >= maxItems:
                    break
        #print(strSimilar)
        sMsg = '~~~%s' % (strSimilarMail)
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
        print(sMsg.encode('utf8'))
            
        sql = "Update tbl_su_user_org Set " \
              "IsSimilarUniList = %s " \
              "Where Source = 'M' " \
              "and ID_University = %s " \
              "and customerid = %s " \
              "and Status = %s "
        #print(sql)
        oConn.executeNonQueryParaAslastrowid(sql, strSimilar, row['ID_University'], row['customerid'], sta_New)   
        
                
    # finish of process  
    sMsg = '%s Total processed = %s.' % (str(datetime.now()), str(i))
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
    
    Common.SysSettings._processuserpostsendlogYN = 'Y'   # co new org la se gui mail
    
    return numberOfOrg

#testing
#ProcessOrgOfUser(getConnectionManual(wAcabeta['dbserver'], wAcabeta['dbname'], wAcabeta['dbuser'], wAcabeta['dbpwd']))


def ProcessPostOfUser(atsite = None):
    # clear old log
    if Common.SysSettings._clearlogbeforeminingYN == 'Y':
        Common.Util.clearLogFile(Common.SysSettings._processuserpostlogFN)
     
    # start capture text at console 
    if Common.SysSettings._captureconsoleYN == 'Y':
        sys.stdout = Common.Util.Log(Common.SysSettings._processuserpostlogFN)
    
    # make connection to site
    oConn = getConnectionManual(atsite['dbserver'], atsite['dbname'], atsite['dbuser'], atsite['dbpwd'])
    # vars
    sta_Reject = 0
    sta_Publish = 1
    sta_Accept = 2
    sta_Ready = 4
    sta_Pending = 5
    sta_New = 8
    
    # ************************ Begin processing org of user on wAcademia ****************************
    uploadcomment = '\nI. Begin process user org %s.\n' % atsite['dbname']
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + uploadcomment
    print(uploadcomment)
    ProcessOrgOfUser(oConn)
    
    
    # ************************* Begin processing post of user on wAcademia ****************************
    uploadcomment = '\nII. Begin process user post: (phase 1) generate data & (phase 2) publish & (phase 3) show free box %s.\n' % atsite['dbname']
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + uploadcomment
    print(uploadcomment)
    
    # get list of new post
    sQuery = "select * from rec_userpostjobs where deleted_at is null and Status = %s" % sta_New
    listOfPost = oConn.selectSql(sQuery)
    numberOfPost = 0 
    if listOfPost:
        numberOfPost = len(listOfPost)
    
    # *************************** phase 1: processing *******************************
    sMsg = '%s Phase 1: Processing new user post: %s' % (str(datetime.now()), str(numberOfPost))
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
    
    if numberOfPost>0:
        Common.SysSettings._processuserpostsendlogYN = 'Y'   # co new post la se gui mail
        
        # object
        oHTMLProcessing = HTMLProcessing()
        oJobProcessing = JobProcessing(oConn)
        oTextProc = TextProcessing(oConn)
        
        # vars
        ResearchAreaList = oJobProcessing.getResearchAreaList()
        i = 0
        r = 0
        p = 0
        for row in listOfPost:
            i += 1
            sMsg = '%s %s/%s: %s' % (str(datetime.now()), i, numberOfPost, row['JobTitle'])
            Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg 
            print(sMsg.encode('utf8'))
            
            # Processing content to generating data
            postid = row['ID']
            htmlcontent = row['HTMLAdvertisement']
            nonhtmlcontent = oHTMLProcessing.cleanHTMLScript(htmlcontent)             
            keywords = getKeywords(nonhtmlcontent)
            abstract = oHTMLProcessing.getJobAbstract(nonhtmlcontent)
            
            listOfResearchArea = oTextProc.classifyReasearchArea(postid, nonhtmlcontent, ResearchAreaList)
            strResearchArea = ""  # like: abc, xyz, mnk
            #itm[0]: ID_ResearchArea
            #itm[1]: ID
            for itm in listOfResearchArea:  
                ID_ResearchArea=itm[0]
                #ID=itm[1]           
                strResearchArea = strResearchArea + ID_ResearchArea + ", "
                
            if len(strResearchArea)>0:
                strResearchArea = strResearchArea[:-2]      # bo di 2 ky tu cuoi '; ' noi vao o tren
            
            deadline = row['Deadline']
            if deadline == None:
                deadline = getDateFormat("")
            
            # check valid
            if row['ID_University'] and row['ID_Position'] and nonhtmlcontent and deadline and strResearchArea and keywords:
                r += 1
                status = sta_Ready           
            else:
                p += 1
                status = sta_Pending
                sMsg = '---------------=> Pending job: %s' % (postid)
                Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
                print(sMsg)
                
            sql = "Update rec_userpostjobs Set " \
                  "NonHTMLAdvertisement = %s, " \
                  "Deadline = %s, " \
                  "Keywords = %s, " \
                  "Abstract = %s, " \
                  "ID_ResearchAreaList = %s, " \
                  "Status = %s " \
                  "Where ID = %s "
            oConn.executeNonQueryParaAslastrowid(sql, nonhtmlcontent, deadline, keywords, abstract, strResearchArea, status, postid)   
    
        # finish of process  
        sMsg = '%s Total processed = %s. Ready = %s and Pending = %s' % (str(datetime.now()), str(i), str(r), str(p))
        Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
        print(sMsg)
    
    
    
    # *************************** phase 2: Publishing *****************************  
    sMsg = '\n\n%s Phase 2: publishing to tbl_positions...' % (str(datetime.now()))
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
    jobid = '' # truyen vao trong se xly toan bo. nguoc lai chi xly rieng id do
    pp = oConn.call_store_procedure('prc_autoPubUserPost', jobid)  
    for row in pp:
        ret = row['ret']
    arr = ret.split('#')
    numberOfPublish = int(arr[1])
    sessionkey = arr[2]
    
    '''
    pp = oConn.call_store_procedure('prc_autoPubUserOrg')
    for row in pp:
        ret = row['ret']
    arr = ret.split('#')
    numberOfPublishOrg = int(arr[1])
    sessionkeyOrg = arr[2]
    '''
    
    sMsg = '%s Finished publish user-post [%s] = %s' % (str(datetime.now()), sessionkey, str(numberOfPublish))             
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
        
    if numberOfPublish>0:
        Common.SysSettings._processuserpostsendlogYN = 'Y'   # co new post la se gui mail
    
    
    # ***************************** phase 3: Show free post in box *****************************
    sMsg = '\n\n%s Phase 3: show user-post to tbl_freepostjobs' % (str(datetime.now()))
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
    pp = oConn.call_store_procedure('prc_autoFreeJobs')  
    for row in pp:
        ret = row['ret']
    arr = ret.split('#')
    numberOfPublish = int(arr[1])
    sessionkey = arr[2]
    
    sMsg = '%s Finished show at freebox [%s] = %s' % (str(datetime.now()), sessionkey, str(numberOfPublish))             
    Common.SysSettings._processuserpostshortlogcontent = Common.SysSettings._processuserpostshortlogcontent + '\n' + sMsg
    print(sMsg)
        
    #if numberOfPublish>0:
    #    Common.SysSettings._processuserpostsendlogYN = 'Y'   # co new post la se gui mail
    
    
    # ***************************** GEN STATISTIC *****************************
    GenStatisticUserPost(oConn)
    
    
    
    # stop capture text at console 
    if Common.SysSettings._captureconsoleYN == 'Y':
        #f = open('nul', 'w') # day stdout ra vung dau do. (https://stackoverflow.com/questions/6735917/redirecting-stdout-to-nothing-in-python)
        #sys.stdout = f
        #sys.__stdout__  # quay ve mac dinh. https://stackoverflow.com/questions/14245227/python-reset-stdout-to-normal-after-previously-redirecting-it-to-a-file
        sys.stdout = sys.__stdout__
    
    # send mail
    if Common.SysSettings._processuserpostsendlogYN == 'Y':
        Common.Util.sendLogFileToUs(Common.SysSettings._processuserpostlogFN, "Lotus engine log of processing user-post", Common.SysSettings._processuserpostshortlogcontent)
    
    return numberOfPost


   
def testmatrix():
    item1 = []
    item1.append('object')
    item1.append(0.2)
    
    item2 = []
    item2.append('object 2')
    item2.append(4.1)
    
    item3 = []
    item3.append('object 3')
    item3.append(3)
    
    li = []
    li.append(item1)
    li.append(item2)
    li.append(item3)
    
    #li = [['2',0.1],['1',3.1],['5',0.4]]

    #li.sort(key=lambda x:x[1])
    li.sort(key=lambda x:x[1], reverse=True)

    print(li) 
    
#testmatrix()
#ProcessPostOfUser(wAcademia)