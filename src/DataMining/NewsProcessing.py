# -*- coding: utf-8 -*-
'''
Created on Jul 31, 2017
@author: thg03
'''

import datetime
import uuid
from DataMining.MachineLearning import TextProcessing
from DataMining.ExtractEmails import ExtractEmailsFromText, ExtractEmailfromJob
from DataMining.ExtractKeyWords import getKeywords
from DataMining.HTMLProcessing import HTMLProcessing
from datetime import timedelta

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import re
from langdetect import detect
from DataMining.NewsLinks import NewsLink


import Common.SysSettings
from Common.Util import getMachineLearningModel
from DataMining.JobDeadline import JobDeadline
from bs4 import BeautifulSoup



class NewsProcessing():
    __oCnn = None
    __f_Vector=None #featurer vector for Machine Learning Model
    __f_MLModel=None
    __dict=None
    __modelML=None#getMachineLearningModel()# Load Machine Learning Model
    __oTextProc=None
    __oJobLink=None
    __backDate=None
    __VIP_Keywords=None
    __oDeadline=None

    
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self, oConn):
        self.__oCnn=oConn
        
        #print(__modelML)
        self.__f_Vector=None#self.__modelML[0]
        self.__f_MLModel=None#self.__modelML[1]
        self.__dict={0: 'PHD', 1: 'ADMIN', 2: 'LECTURER', 3: 'MASTER', 4: 'POSTDOC', 5: 'PROFESSOR', 6: 'RESEARCH', 7: 'STAFF'}
        self.__oTextProc=TextProcessing(self.__oCnn)
        self.__oJobLink=NewsLink(self.__oCnn)
       
        self.__VIP_Keywords=self.load_VIPKeywords()
        
        d = datetime.datetime.today() - timedelta(days=1200)#
        self.__backDate=d.strftime('%Y/%m/%d')
        self.__oDeadline=JobDeadline()
        

    def returnKeyword(self):
        return self.__VIP_Keywords
    
    def returnDeadline(self):
        return self.__oDeadline
        
     
    def getIDPosition(self,sInputText):
        '''
         get Machine Learning Model from file and apply it to classify job.
         return ID_Position
        '''
        
        sInputText=self.getRootWordListFromJob(sInputText)
        raw_documents=[sInputText]

        X_a= self.__f_Vector.transform(raw_documents)
        LinearSVC_classifier=self.__f_MLModel
        y_pred = LinearSVC_classifier.predict(X_a)
        return self.__dict.get(y_pred[0])
        

    def getRootWordListFromJob(self,sDirtyText):
        '''
            Input: Dirty Text
            Output: a text of root word, each one is separated by a space
        '''
    
        punctuation = re.compile(r'[-.?!,":;()|0-9]')
        stop_words = set(stopwords.words('english'))
        lemmatizer = WordNetLemmatizer()    
        
        lstWords=[]  
        word_tokens=word_tokenize(sDirtyText)
        #for s in lst_Sentences:
        #   word_tokens =  word_tokens+ word_tokenize(s)
        for w in word_tokens:
                lowWord=w.lower()
                onlyWord=punctuation.sub("", lowWord)
                if (not onlyWord in stop_words) and (onlyWord.isalpha()):
                    rootWord=lemmatizer.lemmatize(onlyWord)
                    if len(rootWord)>1:
                        lstWords.append(rootWord)
    
        sRootWord=' '.join(lstWords)
        return sRootWord
    
    def updateNumberofLatestNews(self,ID_University,iNumberofRecentPositions):
        '''
         Update number of recent positions and date of update for tbl_University 
         Input: ID_University, Date update data for tbl_Position, field: ProcessingDate and number of position: 
         Outout: number of recent positions.
        '''
        try:
            
            aDateofPost=str(datetime.datetime.now().date()).replace("-","/")
            #print(aDateofPost.strftime('%Y/%m/%d'))
            # Update a record
            sql ="UPDATE tbl_university SET ProcessingDate=%s, NumberofLatestNews=%s WHERE ID_University=%s"
            rowcount = self.__oCnn.executeNonQueryParaAsRowcount(sql, aDateofPost,iNumberofRecentPositions,ID_University)
            if Common.SysSettings._verboselevel > 1:
                print('%s NumberofLatestNews count: %s' % (str(datetime.datetime.now()), iNumberofRecentPositions))
            return True
           
        except NameError:
            print (">>>>>>>>> Error at method: updateNumberofRecentPositions")            
            raise
        return False
    
    
    def updateNumberofRecentPositions(self,ID_University,iNumberofRecentPositions):
        '''
         Update number of recent positions and date of update for tbl_University 
         Input: ID_University, Date update data for tbl_Position, field: ProcessingDate and number of position: 
         Outout: number of recent positions.
        '''
        try:
            
            aDateofPost=str(datetime.datetime.now().date()).replace("-","/")
            #print(aDateofPost.strftime('%Y/%m/%d'))
            # Update a record
            sql ="UPDATE tbl_university SET ProcessingDate=%s, NumberofRecentPositions=%s WHERE ID_University=%s"
            rowcount = self.__oCnn.executeNonQueryParaAsRowcount(sql, aDateofPost,iNumberofRecentPositions,ID_University)
            if Common.SysSettings._verboselevel > 1:
                print('%s updateNumberofRecentPositions count: %s' % (str(datetime.datetime.now()), iNumberofRecentPositions))
            return True
           
        except NameError:
            print (">>>>>>>>> Error at method: updateNumberofRecentPositions")            
            raise
        return False
    
    def insertOnePosition(self, ID_PositionType, aJobTitle, sNonHTML, sHTML, aOriginalLink, aDeadline, ID_University, ID_Country, ResearchAreaList=[]):
        '''
            SQL=
            Purpose: Insert one position into the table: tbl_Positions 
            Input: oConnection, ID_PositionType, aJobTitle, sNonHTML, sHTML, aOriginalLink, aDeadline, ID_University, ID_Country
            Output: True if one position is inserted, False if it is not inserted.
        '''
        try:
            
            #Get ID of Position by UUI
            #ID=str(uuid.uuid4())
            print("------------- BEGIN --------------------------")

            u=str(uuid.uuid1())
            ID = ("%s-%s-%s-%s-%s") % (u[24:],u[14:18],u[9:13],u[0:8],u[19:23])
            oHTMLProcess=HTMLProcessing()
            oJobProcessing=NewsProcessing(self.__oCnn)
            aDateofPost=str(datetime.datetime.now().date()).replace("-","/")
            
            
            
            

            
            ID_Index_JobLink=self.__oJobLink.insert_One_JobLink(ID_University=ID_University, ID_Country=ID_Country, ID_JobLink=ID, Title=aJobTitle.title(), OriginalLink=aOriginalLink, Status=0)
            
            
            isEnglish=detect(sNonHTML)
            if (isEnglish!="en"):
                Status=1# Scan but not process
                self.__oJobLink.update_One_JobLink(ID_University, ID_Index_JobLink, Status)
                return False
            
            
            #Get list of Research Area
            ID_ResearchAreaList=self.__oTextProc.classifyListofReasearchArea(sNonHTML, ResearchAreaList)
            
            
            # Create a new record
            sql = "INSERT INTO tbl_Positions (`ID_Position`, `HTMLAdvertisement`, `OriginalLink`, `Deadline`, `DateofPost`,`JobTitle`,NonHTMLAdvertisement,ID,ID_Country,ID_University,Keywords,Abstract,ID_ResearchAreaList) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            
            
            
            
            
            sAbstract=oHTMLProcess.getJobAbstract(sNonHTML)
            sHTML=sHTML.replace("https://www.nature.com/naturejobs/science/jobs/","")
            sHTML=sHTML.replace("http://www.nature.com/naturejobs/science/jobs/","")
            sHTML=sHTML.replace("https://academicpositions.com/","")
            sHTML=sHTML.replace("http://academicpositions.com/","")
            sHTML=sHTML.replace("academicpositions.com","")
            sHTML=sHTML.replace("http://www.universitypositions.eu","")
            sHTML=sHTML.replace("http://www.phd-jobs.net","")
            
            
            
            txtInputDirtyText=aJobTitle+ ' ' +sNonHTML
            ID_PositionType=self.getIDPosition(txtInputDirtyText)
            
            
            Keywords=getKeywords(text=txtInputDirtyText,VIP=self.__VIP_Keywords,show_score=False, reFormat=0)
            aDeadline=oJobProcessing.returnDeadline().getJobDeadline_Main(sNonHTML, ID_Country) 

                
            
            lastrowid = self.__oCnn.executeNonQueryParaAslastrowid(sql, ID_PositionType,sHTML,aOriginalLink, aDeadline, aDateofPost, aJobTitle.title(),sNonHTML,ID,ID_Country,ID_University,Keywords,sAbstract,ID_ResearchAreaList)
            
            if Common.SysSettings._verboselevel > 1:
                print('%s insertOnePosition lastrowid: %s' % (str(datetime.datetime.now()), lastrowid))            
                print("ID Position:"+ID_PositionType+" Title:"+aJobTitle)
                print("Keywords: "+Keywords)
                print("Deadline:" +aDeadline)    
            
            # Set status in tbl_JobLinks 2- Scanned and Processed. 
            Status=2
            self.__oJobLink.update_One_JobLink(ID_University, ID_Index_JobLink, Status)
            
            
            #Assign Research Area and Position
            
            InsertedResearchArea=self.__oTextProc.classifyReasearchArea(ID, sNonHTML, ResearchAreaList)
            self.__oTextProc.insert_tbl_Positions_ResearchAreas(InsertedResearchArea)
            
            #Exact and Insert Email
            ExtractEmailsFromText(sNonHTML, self.__oCnn)
            return True
        
        except NameError:
            print(">>>>>>>>> Cannot insert: Position:"+ aJobTitle, ", Link:" +aOriginalLink)
            print (">>>>>>>> Error at JobProcessing Class, method insertOnePosition")
            
            #Save to log files            
            pass
        return False
    
    def getNewPositions(self, ID_University, WebLink):
        '''
            Input:  oConnection, ID_University and list of current positions in the website of university.
            Output: list of new positions needs to be inserted into the database.
        '''
        try:
            #sql="SELECT ProcessingDate, ID_University FROM tbl_University WHERE ID_University=%s"
            #aDateofPost = self.__oCnn.selectSqlFetOnePara(sql, ID_University)
            #print(len(aDateofPost))
            
            #sql="SELECT OriginalLink FROM tbl_Positions WHERE ID_University=%s and DateofPost=%s"  
            #table = self.__oCnn.selectSqlPara(sql, ID_University, aDateofPost['ProcessingDate'])
            #sql="SELECT OriginalLink FROM tbl_Positions WHERE ID_University=%s"
            
            isIDJob=False# 4- Khong co ID Job, 4- Co ID Job

            #print(WebLink)
            if(len(WebLink[0])==4):                 
                isIDJob=True # 3- Co ID Job de dinh danh
            
            #d = datetime.datetime.today() - timedelta(days=1095)# 
           
            #sDate= d.strftime('%Y/%m/%d')
            sDate=self.__backDate
            
            #Jobhula & Webacademic
            #Liet ke cac job trong vong 1 nam gan day nhat
            sql="SELECT OriginalLink FROM tbl_Positions WHERE ID_University=%s and DateofPost >=%s"
            sql=sql + " Union "
            sql=sql + "SELECT OriginalLink FROM webacademia.tbl_Positions WHERE ID_University=%s and DateofPost >=%s"
            table = self.__oCnn.selectSqlPara(sql, ID_University, sDate, ID_University, sDate)
            
            #Put data from Database into a List
            listDB=[row['OriginalLink'] for row in table]
            
                        
            if(isIDJob==False):                
                #Scan data from web and compare with DBSet
                
                lstPosition=[WebLink[i] for i in range(len(WebLink)) if(WebLink[i][2] not in listDB)]
            else:
                sListDB=str(listDB)
                lstPosition=[WebLink[i] for i in range(len(WebLink)) if(sListDB.find(str(WebLink[i][3]))==-1)]
            return lstPosition
        except NameError:
            print (">>>>>>>> Error at JobProcessing Class, method getNewPositions()")            
            #Save to log files            
            pass
    
    def getNewPositionSession(self,ID_University,LinkList):
        '''
            Input:  oConnection, ID_University and list of current positions in the website of university.
            Output: list of new positions needs to be inserted into the database.
        '''
        try:
            
            #sql="SELECT ProcessingDate, ID_University FROM tbl_University WHERE ID_University=%s"
            #aDateofPost = self.__oCnn.selectSqlFetOnePara(sql, ID_University)
            #print(len(aDateofPost))
            
            #sql="SELECT OriginalLink FROM tbl_Positions WHERE ID_University=%s and DateofPost=%s"  
            #table = self.__oCnn.selectSqlPara(sql, ID_University, aDateofPost['ProcessingDate'])
            d = datetime.datetime.today() - timedelta(days=150)
            sDate= d.strftime('%Y/%m/%d')
            
            
            #Jobhula
            sql0="SELECT OriginalLink FROM tbl_Positions WHERE ID_University=%s and DateofPost >=%s"
            table0 = self.__oCnn.selectSqlPara(sql0, ID_University,sDate)
            #Webacademic
            sql1="SELECT OriginalLink FROM webacademia.tbl_Positions WHERE ID_University=%s and DateofPost >=%s"
            table1 = self.__oCnn.selectSqlPara(sql1, ID_University,sDate)
            #print(len(table))
            table=table0+table1
            
            
            lstPosition=[]# Empty list for position
            listDB=[]# Empty set
                #Put data from Database into a list
            for row in table:
                listDB.append(row['OriginalLink'])
            sListDB=str(listDB)
                #Scan data from web and compare with DBSet   
            for i in range(len(LinkList)):
                if(sListDB.find(str(LinkList[i][3]))==-1):
                    lstPosition.append(LinkList[i])
            return lstPosition
        except NameError:
            print (">>>>>>>> Error at JobProcessing Class, method getLatestPositions()")            
            #Save to log files            
            pass
        
    def getResearchAreaList(self):
        '''
          GET LIST OF ID_RESEARCHAREA
        '''
        try:
            
            # check caches
            if not Common.SysSettings._listofID_ResearchArea:
                sql="SELECT ID_ResearchArea  FROM tbl_researcharea"
            
                table = self.__oCnn.selectSqlPara(sql)
                lstResearchArea=[]
                for row in table:
                    lstResearchArea.append(row['ID_ResearchArea'])
                
                print('%s Init and cache list of id_researcharea in JobProcessing' % (str(datetime.datetime.now())))
                Common.SysSettings._listofID_ResearchArea = lstResearchArea[:]
                
            return Common.SysSettings._listofID_ResearchArea
        except NameError:
            print (">>>>>>>> Error at JobProcessing Class, method getLatestPositions()")            
            #Save to log files            
            pass
    
    def getJobLocation(self,ID_University):
        
        try:
            
            # check caches
            sql="SELECT  `UniversityAddr` FROM  `tbl_university` WHERE ID_University = %s "
            
            table = self.__oCnn.selectSqlPara(sql,ID_University)            
            #listDB=[row['UniversityAddr'] for row in table]
            table[0]['UniversityAddr']
#           print(table[0]['UniversityAddr'])
            return table[0]['UniversityAddr']

        except NameError:
            print (">>>>>>>> Error at JobProcessing Class, method getJobLocation()")            
            #Save to log files            
            pass
    
    
    def insertOnePosition_into_tbl_News_RawData(self,ID_News,NewsTitle, DateofPost,Summary, Keyword,Top_Image, NonHTML, HTML, OriginalLink, ID_University, ID_Country, open_original_link, Status=2):
        '''
            Purpose: Insert one position into the table: tbl_News
            Input: oConnection, aNewsTitle, sNonHTML, sHTML, aOriginalLink, ID_University, ID_Country
            Output: True if one position is inserted, False if it is not inserted.
        '''
        try:
            sql = "INSERT INTO news_posts(id_country, id_university, guid, title, image_url, plain_text,content, keywords, abstract, orginal_link, status, created_at,open_original_link) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            lastrowid = self.__oCnn.executeNonQueryParaAslastrowid(sql, ID_Country, ID_University, ID_News, NewsTitle,Top_Image, NonHTML, HTML, Keyword, Summary, OriginalLink, Status, DateofPost,open_original_link)
            

            
            if Common.SysSettings._verboselevel > 1:
                print('- %s: Lastrowid: %s' % (str(datetime.datetime.now()), lastrowid))            
                print("- Inserted one News with Title:"+NewsTitle)
            return True
        
        except TypeError as e:
            print(e)

        #except NameError:
            print(">>>>>>>>> Cannot insert:"+ NewsTitle, ", Link:" +OriginalLink)
            print (">>>>>>>> Error at NewsProcessing Class, method insertOnePosition_into_tbl_News_RawData")
            
            #Save to log files            
            pass
        return False
    
    def load_VIPKeywords(self):
        
        kw_VIP=[]
        
        sSQL="SELECT ListKeyword FROM tbl_keywords"        
        fullTable = self.__oCnn.selectSqlPara(sSQL)
        for row in fullTable:
            sKeyWords=str(row['ListKeyword']).lower()
            if (sKeyWords.find(",")>0):
                lstKeywords = [x.strip() for x in sKeyWords.split(',')]
                kw_VIP=kw_VIP+lstKeywords
            else:
                kw_VIP.append(sKeyWords)

        return kw_VIP
