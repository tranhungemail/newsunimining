'''
Created on Sep 5, 2017

@author: thg03
'''
import datetime
import DataMining.NewsProcessing
from contextlib import suppress
import Common.SysSettings

class TextProcessing():
    __oCnn = None
    
    def __init__(self, oConn):
        self.__oCnn = oConn
            
    def classifyReasearchArea(self,ID_Job, txtNonHTMLJob,lstResearchArea=[]):
        '''
        This method is used to classify a job into ResearchArea
        
        Input: - txtJob is a plain text
               - Long research area List from tbl_positiontype
        Output: 
                 return a table having to colums: ID_ResearchArea and ID_Job 
        
        '''
        #PROCESS TEXT
        dicResearchArea={}        
        words=txtNonHTMLJob.split(" ")    
        words=[w.upper().strip() for w in words]
        txtJob=" ".join(words)
        
        #MAKE AN STATISTICS
        for aKey in lstResearchArea:
            dicResearchArea[aKey]=txtJob.count(aKey)
        
        # MAKE A LIST OF ID_ResearchArea, ID
        finalList=[]
        for ID_ResearchArea in dicResearchArea:  
            interList=[]
            if dicResearchArea[ID_ResearchArea] >0:
                interList.append(ID_ResearchArea)#ID_ResearchArea
                interList.append(ID_Job)#ID of Job
                finalList.append(interList)
        return  finalList
    
    
    def classifyListofReasearchArea(self, txtNonHTMLJob,lstResearchArea=[]):
        '''
        This method is used to classify a job into ResearchArea
        
        Input: - txtJob is a plain text
               - Long research area List from tbl_positiontype
        Output: 
                 return a string of ResearhArea, Each separate by a comma like CHEMIST, EDUCATION, ENGINEER, PHYSIC
        
        '''
        #PROCESS TEXT
        dicResearchArea={}        
        words=txtNonHTMLJob.split(" ")    
        words=[w.upper().strip() for w in words]
        txtJob=" ".join(words)
        
        #MAKE AN STATISTICS
        for aKey in lstResearchArea:
            dicResearchArea[aKey]=txtJob.count(aKey)
        
        # MAKE A LIST OF ID_ResearchArea, ID
        finalList=[]
        for ID_ResearchArea in dicResearchArea:  
            if dicResearchArea[ID_ResearchArea] >0:
                finalList.append(ID_ResearchArea)#ID_ResearchArea
        sResearchArea=', '.join(finalList)
        return  sResearchArea
    
    def insert_tbl_Positions_ResearchAreas(self,shortListResearchAreas=[]):
        '''
            Purpose: Insert a ID and its research area of a position into the table: tbl_Positions_ResearchAreas
            Input: shortListResearchAreas (get from classifyReasearchArea()): ID_ResearchArea, ID
            Output: True if the insert successfully, False if it is not inserted.
        '''
        
        try:
            #itm[0]: ID_ResearchArea
            #itm[1]: ID
            for itm in shortListResearchAreas:  
                ID_ResearchArea=itm[0]
                ID=itm[1]           
                sql = "INSERT INTO tbl_positions_researchareas(ID_ResearchArea, ID ) VALUES (%s,%s)"
                self.__oCnn.executeNonQueryParaAslastrowid(sql,ID_ResearchArea, ID)
                if Common.SysSettings._verboselevel > 1:
                    print('%s Insert tbl_positions_researchareas, ID_ResearchArea: %s - ID Job: %s' % (str(datetime.datetime.now()), ID_ResearchArea,ID))                    

            return True        
        except NameError:
            print("Cannot insert:"+ID_ResearchArea+":"+ID )
            print ("Error at JobProcessing Class, method insert_tbl_Positions_ResearchAreas")            
            #Save to log files            
            pass
        return False
    
    def update_tbl_Positions_ResearchAreas(self,shortnameCountry):
        '''
            IMPORTANT NOTE: This method may take long time as  records are huge
            Purpose: classify job into research area.
            Input: shortname of country (three capital letter)
            Output: Results will be inserted into tbl_positions_researchareas
        '''
        
        try:            
            
            tbl_positions =self.__oCnn.select('tbl_positions', "ID_Country='"+shortnameCountry +"'",'ID','NonHTMLAdvertisement')            
            oJobProc=DataMining.NewsProcessing.JobProcessing(self.__oCnn)
            
            lstResearchArea=oJobProc.getResearchAreaList()

            for row in tbl_positions:
                ID_Job=row['ID']
                txtNonHTMLJob=row['NonHTMLAdvertisement']                    
                shortListResearchAreas=self.classifyReasearchArea(ID_Job, txtNonHTMLJob, lstResearchArea)
                with suppress(Exception):
                    self.insert_tbl_Positions_ResearchAreas(shortListResearchAreas)
        except NameError:
            print ("Error at TextProcessing Class, method update_tbl_Positions_ResearchAreas()")            
            #Save to log files            
            pass

