'''
keywords_extraction v1.0
24.05.2019

@author: Hungdo
'''
"""
keyword_extraction
    - Input:  
        + text (non html) 
        + VIP : list of VIP key words def: Null
        
        + min_score : min score to consider as key words def: 1.5
        + show_socre : show score in list def: False
        
    - Output: list of key words
    
"""
#%%
import RAKE
import re
from nltk.stem.wordnet import WordNetLemmatizer


def keywords_extraction(text="", VIP=[], min_score=1, show_score=False):
    # Clean text
    text = text.lower()
    #Convert to lowercase
    text = re.sub(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+","", text) # remove email
    text = re.sub(r"http://+[a-z0-9\.\-+_/]+|https://+[a-z0-9\.\-+_/]+|www\.+[a-z0-9\.\-+_/]+","",text) #remove weblink
    text = re.sub(r'[^a-z()\'\"\.\,]', ' ', text) 
    
    text = text.split()
    
    #Lemmatisation 
    lem = WordNetLemmatizer()
    text = [lem.lemmatize(word) for word in text] 
    text = " ".join(text)
    #print(text)
    rake = RAKE.Rake(RAKE.SmartStopList())
    rake_keywords = rake.run(text, minCharacters = 3, maxWords = 3, minFrequency = 1)
    #print(rake_keywords)
    if rake_keywords!=[]: 
        inc=rake_keywords[0][1]/3
    else: inc=0
    #print(inc)
    # if key word is VIP, increase score
    new_rake=[]
    k=0
    for w in rake_keywords:
        new_rake.append(list(w))
        for i in VIP: 
            if i.lower() in w[0]: 
                new_rake[k][1]=rake_keywords[k][1]+inc 
        k+=1
    
    # sort the keywords
    def takeSecond(elem):
        return elem[1]
    new_rake.sort(key=takeSecond,reverse=True)

    # take the keyword with score > min_score
    rake_keywords=[]
    if show_score:
        for w in new_rake:
            if w[1]>=min_score:
                rake_keywords.append(w)
    else:
        for w in new_rake:
            if w[1]>=min_score:
                rake_keywords.append(w[0])

    return(rake_keywords)



#%% Test
#import pandas

#missing_values = ["n/a", "na", "NA"]
#dataset = pandas.read_csv('academicgates\positions_Hung.csv', encoding='utf-8', na_values=missing_values)
#data = dataset.NonHTMLAdvertisement

#data1=(pandas.read_csv('ap_researcharea.csv'))
#data2=(pandas.read_csv('ap_researcharea_subitem.csv'))
#vip1=list(data1.ResearchArea)
#vip2=list(data2.Title)
#vip3=["phd","research assistant","part time", "full time","doctoral","postdoc","master","undergaduate","postgraduate"]

#VIP=vip1+vip2+vip3

#keywords=keywords_extraction(text=data[4],VIP=VIP,min_score=3,show_score=True)
#print(keywords)

