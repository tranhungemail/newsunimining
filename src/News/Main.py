# -*- coding: utf-8 -*-
'''
Created on Nov 10, 2020

@author: lapbt

Maining news

'''
import sys
# fix error Import no module named: https://stackoverflow.com/questions/15514593/importerror-no-module-named-when-trying-to-run-python-script
sys.path.append('C:\\Lotus\\Engine_Python\\NewsUniMining\\src')

# Remove all tag accept img https://stackoverflow.com/questions/699468/remove-html-tags-not-on-an-allowed-list-from-a-python-string
# https://stackoverflow.com/questions/7470333/remove-certain-attributes-from-html-tags
#https://stackoverflow.com/questions/3073881/clean-up-html-in-python
from Common.WebsiteInfo import *
from Common.TaskMan import TaskManage
import datetime
import Common.Util
import Common.SysSettings

import DrudgeReport.DailyNews_Main
import UniNews.UniNews_Main


# clear old log
if Common.SysSettings._clearlogbeforeminingYN == 'Y':
    Common.Util.clearLogFile(Common.SysSettings._mininglogFN)

# start capture text at console 
if Common.SysSettings._captureconsoleYN == 'Y':
    sys.stdout = Common.Util.Log(Common.SysSettings._mininglogFN)

startat = datetime.datetime.now()
nowhour = startat.hour

# Task run at W10
mining_UK_time = 4
mining_EU_time = 4
mining_EU_quota = 1
mining_EU_order = ''


if nowhour == mining_EU_time or True:
    # Chau Au: EU
    hourquota = mining_EU_quota
    miningat = '%s I. Begin mining news of Uni & Drudge at %s --> %s = %s.\n' % (mining_EU_order, str(startat.strftime("%H:%M")), str((startat + datetime.timedelta(hours=hourquota)).strftime("%H:%M")), str(hourquota))
    print(miningat)
    Common.SysSettings._shortlogcontent = Common.SysSettings._shortlogcontent + '\n' + miningat
            
    try:
        # note log
        telog = TaskManage(website = lJobhula)
        tecode = 'M-News'
        telog.startTask(taskcode=tecode, taskname='Mining news of Uni & Drudge', taskcontent=str(nowhour))
        UniNews.UniNews_Main.main()     

        DrudgeReport.DailyNews_Main.main()
        
        
        telog.finishTask(taskcode=tecode)  

    except:
        Common.Util.sendExceptionErrorEmail(Common.SysSettings._shortlogcontent, "MainingNews.main().EU")
    

elif nowhour == mining_UK_time:
    print('none')
    

# show total mining
totalnew = '\n%s Total new news are inserted: %s <========' % (str(datetime.datetime.now()), str(Common.SysSettings._miningnewpost))
print(totalnew)
Common.SysSettings._shortlogcontent = Common.SysSettings._shortlogcontent + '\n' + totalnew

# stop capture text at console 
if Common.SysSettings._captureconsoleYN == 'Y':
    #f = open('nul', 'w') # day stdout ra vung dau do. (https://stackoverflow.com/questions/6735917/redirecting-stdout-to-nothing-in-python)
    #sys.stdout = f
    sys.__stdout__  # quay ve mac dinh. https://stackoverflow.com/questions/14245227/python-reset-stdout-to-normal-after-previously-redirecting-it-to-a-file
    
# send mail
if Common.SysSettings._sendmailtousYN == 'Y':
    Common.Util.sendLogFileToUs(Common.SysSettings._mininglogFN, Common.SysSettings._mailsubject_AcaMiningNews, Common.SysSettings._shortlogcontent)
    







