# -*- coding: utf-8 -*-
'''
Created on Sep 24, 2017

@author: thg03
'''

from News.DrudgeReport.DailyNews_Universities import *
from News.DrudgeReport.DailyNews_UniversityInfo import *


import Common.SysSettings
from DataMining.NewsProcessing import NewsProcessing
from DataAccess.MakeConnect import getConnection
from _datetime import datetime  # waring: fai de imp cu the o dong duoi import *
from selenium.webdriver.common.by import By
from contextlib import suppress

from DataMining import NewsLinks
import uuid
from DataMining.NewsLinks import NewsLink
from langdetect import detect
import re


if __name__ == '__main__':
    pass


'''
    inp: dict of univesity information
    out: the object of university
'''
def getUniversityObject(**university):
    
    if university['id'] == iDrugeReportNews['id']:
        oGU = DrugeReportNews()
        
    return oGU
           

def main():
    print("==================== USA_DrudgeReport ====================")
    try:
        # create new connection
        oConnection = getConnection()       
        #mining(oConnection, **iDrugeReportNews)
        with suppress(Exception):
            mining(oConnection, **iDrugeReportNews)
        
        oConnection._close()
        

    except Exception as e:
        print(">>>>>>>>> Error at function main: iDrugeReportNews. Error: " + str(e))

def mining(oConnection, **university):
    # check result of last university
    '''
     This function is to mining data from websites and put them into database
     Steps are as follows:
     1- Scan news from website of universities and organizations
     2- Compare with the existence link in the databases and get the ones which are not in the database.
     3- Put these links into the database and set the flag for these links Status=0 (New)
     4- Crawled data from these links which has Status =0. 
        If its contents is English, 
           Insert this Content into Database tbl_News and set JobStatus=9
           Update tbl_newslinks.Status=2 (News is mined)
        else
           tbl_joblinks.Status=1 (not processed further due to non-english) 
    
    '''
    
    if Common.SysSettings._miningcurrent != "":
        errorat = ">>>>>>>>>>>>>> Mining error at %s <<<<<<<<<<<<<<<<<<" % Common.SysSettings._miningcurrent 
        Common.SysSettings._shortlogcontent = Common.SysSettings._shortlogcontent + '\n' + errorat
        print(errorat)
        
    print("------------- BEGIN ---------------------")
    
    try:        
        print('%s Working at: %s... Country ID: %s' % (str(datetime.now()), university['name'],university['countryid']))
        Common.SysSettings._miningcurrent = university['id']        # set to current
        
        oJobProcessing =  NewsProcessing(oConnection)
        oNewsLinks= NewsLink(oConnection)   
        oGU = getUniversityObject(**university)

        
        ID_Country=university['countryid']
        ID_University=university['id']        
        #aJobLocation=oJobProcessing.getJobLocation(ID_University)
        
        #Get data from the website
        tbl_WebsiteJobLinks=oGU.getTable_NewsLinks() 
        #Get only new links from website        
        lst_New_Links_from_Website=oNewsLinks.getNewNewsLinks(ID_University, tbl_WebsiteJobLinks)
        
                
        NumberPosition=len(lst_New_Links_from_Website)# Real new number of positions
        print('%s There are %s new news in the website.' % (str(datetime.now()), str(NumberPosition)))        
        
        
        countInsertedPositions=0
        
        for i in range(NumberPosition):
            
            print("------"+str(datetime.now())+"-------Mining "+str((i+1))+"/"+str(NumberPosition)+" (total news)----------------------")
            
            aNewsTitle=lst_New_Links_from_Website[i][0]
            aOriginalLink= str(lst_New_Links_from_Website[i][2])# Full Original Link
            #Setting job ID
            u=str(uuid.uuid1())
            ID_NewsLink = ("%s-%s-%s-%s-%s") % (u[24:],u[14:18],u[9:13],u[0:8],u[19:23])
            
            
            if Common.SysSettings._verboselevel > 2:
                    print('%s Mining at link: %s.' % (str(datetime.now()), aOriginalLink))
            
            
            NewsContents=oGU.getContentNews(aOriginalLink) #  Pass link to method to get detail position
            isInserted=False
            with suppress(Exception):

                aNewsTitle=NewsContents[0]#txtTitle
                aDateofPost=NewsContents[1]#txtDateOfPost
                aOriginalLink=NewsContents[2]#txtJobLink
                txtImgLink=NewsContents[3]#txtImgLink            
                txtSummary=NewsContents[4]#txtSummary
                txtKeyword=', '.join(NewsContents[5])#txtKeyword
                aHTMLContent=NewsContents[6]#txtHTML
                aNonHTMLContent=NewsContents[7]#txtText
                open_original_link=NewsContents[8]
                
            # edited by lapbt. 07-11-2022. K0 lay news co title trong
            if aNewsTitle != "":
                oNewsLinks.insert_One_NewsLink(ID_University=ID_University, ID_Country=ID_Country, ID_NewsLink=ID_NewsLink, Title=aNewsTitle, OriginalLink=aOriginalLink, Status=2)
    
                with suppress(Exception):
                    isInserted=oJobProcessing.insertOnePosition_into_tbl_News_RawData(ID_News=ID_NewsLink,NewsTitle=aNewsTitle, DateofPost = aDateofPost,Summary= txtSummary, Keyword=txtKeyword,Top_Image=txtImgLink, NonHTML=aNonHTMLContent, HTML=aHTMLContent, OriginalLink=aOriginalLink, ID_University=ID_University, ID_Country=ID_Country,open_original_link=open_original_link)
                
                if isInserted==True:
                    countInsertedPositions=countInsertedPositions+1
                        
        if countInsertedPositions>0: 
            oJobProcessing.updateNumberofLatestNews(university['id'], countInsertedPositions)
        print('- There are %s new news in the website and %s are inserted into database.' % (str(NumberPosition),str(countInsertedPositions)))
        
        # end of mining
        Common.SysSettings._miningcurrent = ""                      # clear done
        Common.SysSettings._miningnewpost += countInsertedPositions     # total news
    except Exception as e:
        print(">>>>>>>> Error at function mining: " + university['name'] + ". Error: " + str(e) + ". Country: "+university['countryid'])
        pass
    
    print("------------- END --------------------------")
    return countInsertedPositions   

    
#MAIN

# Lapbt ---> Luu y khi dung lenh print o cac cho. KHONG print truc tiep, neu k0 qtrong. Neu debug thi lam xong fai closed lai

#print(main())
# Dang sua doi tai day
# END OF MAIN




