# -*- coding: utf-8 -*-
'''
Created on Sep 24, 2017

@author: thg03
'''


from Common.FormatUtil import *
from News.DrudgeReport.DailyNews_UniversityInfo import *
from Common.Util import *

from bs4 import BeautifulSoup
import urllib.request
from DataMining.HTMLProcessing import HTMLProcessing
from DataMining.NewsProcessing  import NewsProcessing
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from _datetime import datetime
from urllib3.util import url

import newspaper
from newspaper import Article


from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import json 

from selenium.webdriver.common.keys import Keys
#from zope.interface.ro import rof
from newspaper.configuration import ArticleConfiguration
from logging import exception




class DrugeReportNews():
    __homelink = iDrugeReportNews['homelink']
    __newslink = iDrugeReportNews['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iDrugeReportNews['homelink']
        __newslink = iDrugeReportNews['newslink']
        
        print('%s Init iDrugeReportNews ok' % str(datetime.now()))
 
    def getTable_NewsLinks(self,num_news=3):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink
        
        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(3)
        
        list_news=oDriver.find_elements_by_partial_link_text("...")
        
        

       
        
        
        n=min(num_news, len(list_news))
        final_list = []#
        
        for article in list_news[0:n]:
            
            
            intermediate_list = []
            
            ID_News=article.get_attribute("href")
            if(ID_News.find("https://www.msn.com/en-us/news/good-news")==-1):
                articleContent = Article(ID_News, language='en')
                try:
                    articleContent.download()
                    articleContent.parse()
                    
                    txtTitle=articleContent.title
                    txtDeadline=getDateFormat("")            
                    txtJobLink=articleContent.url
                    
                   
                    intermediate_list.append(txtTitle)#Title of News
                    intermediate_list.append(txtDeadline)
                    intermediate_list.append(txtJobLink)
                    intermediate_list.append(ID_News)#ID_News
                    final_list.append(intermediate_list)
    
                except:
                    pass               
            
        oDriver.quit()

        return final_list 
    
    
    
    
    def getContentNews(self,link,open_original_link=1):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        oHTML=HTMLProcessing()
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            txtHTML=  oHTML.clean_HTML4News(articleContent.article_html) 
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#open_original_link

            
            return JobDetail
        except :
            print("Errors at Drudge report function getContentNews ")
            
            pass
        return JobDetail
            

