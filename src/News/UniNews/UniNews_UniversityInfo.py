# -*- coding: utf-8 -*-
'''
Created on Sep 24, 2017

@author: thg03

'''


iMonashUniversity = {
  'id': 'AUS_MOUN',
  'name': 'Monash University',
  'homelink': 'http://www.monash.edu.au/',
  'newslink': 'http://careers.pageuppeople.com/513/cw/en/listing/?page=1&page-items=200',
  'countryid': 'AUS'
}

iUniversityofSydney= {
  'id': 'AUS_UNOFSY',
  'name': 'University of Sydney',
  'homelink': 'http://www.usyd.edu.au/',
  'newslink': 'https://www.sydney.edu.au/news-opinion/news/science-and-technology.html',
  'countryid': 'AUS',
  'rootnews':""

}

iUniversityofNewSouthWales = {
  'id': 'AUS_UNOFNESOWA',
  'name': 'University of New South Wales',
  'homelink': 'http://www.unsw.edu.au/',
  'newslink': 'https://newsroom.unsw.edu.au/search',
  'countryid': 'AUS',
  'rootnews':"https://newsroom.unsw.edu.au/"
}

iUniversityofMelbourne = {
  'id': 'AUS_UNOFME',
  'name': 'University of Melbourne',
  'homelink': 'http://www.unimelb.edu.au/',
  'newslink': 'https://about.unimelb.edu.au/newsroom/all-news',
  'countryid': 'AUS',
  'rootnews':""
}


iUniversityofSouthAustralia= {
  'id': 'AUS_UNOFSOAU',
  'name': 'University of South Australia',
  'homelink': 'http://www.unisa.edu.au/',
  'newslink': 'https://www.unisa.edu.au/media-centre/Releases/',
  'countryid': 'AUS',
  'rootnews': 'https://www.unisa.edu.au/',
}


iAustralianNationalUniversity = {
  'id': 'AUS_AUNAUN',
  'name': 'Australian National University',
  'homelink': 'http://www.anu.edu.au/',
  'newslink': 'https://www.anu.edu.au/news/all-news',
  'countryid': 'AUS',
  'rootnews': 'https://www.anu.edu.au/'

}

iBondUniversity = {
  'id': 'AUS_BOUN',
  'name': 'Bond University',
  'homelink': 'http://www.bond.edu.au/',
  'newslink': 'https://bond.edu.au/news?category=2',
  'countryid': 'AUS',
  'rootnews': 'http://bond.edu.au'
}


iNewYorkUniversity = {
    "id": "USA_NY_NEYOUN",
    "name": "New York University",
    "homelink": "https://www.nyu.edu",
    "newslink": "https://www.nyu.edu/about/news-publications/news/search.html",
    "countryid": "USA",
    "rootnews": "https://www.nyu.edu/"
}


iEngineeringInstituteofTechnology={
     #Customer
  'id': 'dd149b20-fa8b-11eb-86a9-4fc744a5d9b1',
  'name': 'Engineering Institute of Technology',
  'homelink': 'https://www.eit.edu.au/',
  'newslink': 'https://www.eit.edu.au/news-events/news/?category%5B%5D=article',
  'countryid': 'AUS',
  'rootnews': ''
}

iPhenikaaUniversity={
     #Customer
  'id': '76142fb0-98df-11ea-ac31-4d8955fff023',
  'name': 'Phenikaa University',
  'homelink': 'https://www.phenikaa-uni.edu.vn',
  'newslink': 'https://phenikaa-uni.edu.vn/en/category/newswire',
  'countryid': 'VNM',
  'rootnews': 'https://phenikaa-uni.edu.vn'
}


iDuyTanUniversity={
     #Customer
  'id': '2d194e70-bd86-11eb-8539-5f35d6951aa4',
  'name': 'Duy Tan University',
  'homelink': 'https://duytan.edu.vn/',
  'newslink': 'https://duytan.edu.vn/news/NewsCategory.aspx?pid=2066&page=0&lang=en-US',
  'countryid': 'VNM',
  'rootnews': 'https://duytan.edu.vn/news/'
}

iVancouverIslandUniversity= {
    #Customer
  'id': 'CAN_VAISUN',
  'name': 'Vancouver Island University',
  'homelink': 'https://www.viu.ca/',
  'newslink': 'https://news.viu.ca/research',
  'countryid': 'CAN',
  'rootnews': ''
}


iETHZurich= {
  'id': 'CHE_ETHZU',
  'name': 'ETH Zurich',
  'homelink': 'https://www.ethz.ch/',
  'newslink': 'https://ethz.ch/en/news-and-events/eth-news.html',
  'countryid': 'CHE',
  'rootnews': "https://www.jobs.ethz.ch"
}

iUniversityofBergen = {
  'id': 'NOR_UNOFBE',
  'name': 'University of Bergen',
  'homelink': 'http://www.uib.no',
  'newslink': 'https://www.uib.no/en/news/news-archive',
  'rootnews':'https://www.uib.no/',
  'countryid': 'NOR'
}

iUniversityofOslo = {
  'id': 'NOR_UNOFOS',
  'name': 'University of Oslo',
  'homelink': 'https://www.uio.no/english/',
  'newslink': 'https://www.uio.no/english/research/news-and-events/news/',
  'countryid': 'NOR'
}

iTheUniversityofTokyo = {
  'id': 'JPN_THUNTO',
  'name': 'The University of Tokyo',
  'homelink': 'https://www.u-tokyo.ac.jp/focus/en',
  'newslink': 'https://www.u-tokyo.ac.jp/focus/en/press/index.php?scrollTop=300&type=Research+news&busho=&year=',
  'countryid': 'JPN',
  'rootnews':'https://www.u-tokyo.ac.jp/'
}


iTechnicalUniversityofDenmark = {
  'id': 'DNK_TEUNOFDE',
  'name': 'Technical University of Denmark',
  'homelink': 'http://www.dtu.dk',
  'newslink': 'https://www.dtu.dk/english/news',
  #'joblink':'http://www.dtu.dk/english/about/job-and-career/vacant-posistions?fr=1&jobpage=53dacb7b-8cc6-4176-a48b-9060cb93b72a&mr=100&qt=JobQuery#jobFilter',
  'countryid': 'DNK',
  'rootnews':'https://www.dtu.dk/'
}

iUniversityofCopenhagen = {
  'id': 'DNK_COUN',
  'name': 'University of Copenhagen',
  'homelink': 'http://www.ku.dk/',
  'newslink': 'https://news.ku.dk/all_news/',
  'countryid': 'DNK',
  'rootnews':'https://news.ku.dk/'
}

iAaltoUniversity = {
  'id': 'FIN_AAUN',
  'name': 'Aalto University',
  'homelink': 'http://www.aalto.fi',
  'newslink': 'https://www.aalto.fi/en/news?field_news_category_target_id[1731]=1731',
  'countryid': 'FIN',
  'rootnews':'https://www.aalto.fi/',
}


iHelsikiUniversity = {
  'id': 'FIN_HEUNOFTE',
  'name': 'Helsinki University',
  'homelink': 'https://www.helsinki.fi/',
  'newslink': 'https://www.helsinki.fi/en/news/news-and-press-releases?tid=All&field_news_item_article_type_tid=1&listing=2',
  'countryid': 'FIN',
  'rootnews':''
}



iTheHongKongUniversityofScienceandTechnology= {
  'id': 'HKG_THHOKOUNSCTE',
  'name': 'The Hong Kong University of Science and Technology',
  'homelink': 'https://www.ust.hk',
  'newslink': 'https://www.ust.hk/news/research-and-innovation',
  'countryid': 'HKG',
  'rootnews': 'https://www.ust.hk/'
}

iTechnischeUniversitatMunchen= {
  'id': 'DEU_TEUNMU',
  'name': 'Technische University Munchen',
  'homelink': 'https://www.tum.de',
  'newslink': 'https://www.tum.de/nc/en/about-tum/news/press-releases/',
  'countryid': 'DEU',
  'rootnews': ''
}



iLudwiMaximiliansUniversitatMunchen = {
  'id': 'DEU_LUMAUNMU',
  'name': 'Ludwig-Maximilians-Universitat Munchen',
  'homelink': 'https://www.en.uni-muenchen.de',
  'newslink':'https://www.en.uni-muenchen.de/news/index.html',
  'rootnews':"https://www.en.uni-muenchen.de/news/",
  'countryid': 'DEU'
}

iUniversityofWageningen = {
  'id': 'NLD_WAUN',
  'name': 'Wageningen University',
  'homelink': 'http://www.wur.nl',
  'newslink':'https://www.wur.nl/en/news-wur.htm',
  'rootnews':"https://www.wur.nl/",
  'countryid': 'NLD'
}


iUniversityofWageningen = {
  'id': 'NLD_WAUN',
  'name': 'Wageningen University',
  'homelink': 'http://www.wur.nl',
  'newslink':'https://www.wur.nl/en/news-wur.htm',
  'rootnews':"https://www.wur.nl/",
  'countryid': 'NLD'
}




iTheUniversityofBritishColumbia= {
  'id': 'CAN_UNOFBRICOL',
  'name': 'The University of British Columbia',
  'homelink': 'https://www.ubc.ca/',
  'newslink': 'https://news.ubc.ca/category/latest-news/',
  'countryid': 'CAN',
  'rootjob': ''
}

iMcGillUniversity = {
    #Good Find Next Find Input
  'id': 'CAN_MCGILLUNI',
  'name': 'Mc Gill University',
  'homelink': 'https://www.mcgill.ca/',
  'newslink': 'https://www.mcgill.ca/newsroom/channels_item/19',  
  'countryid': 'CAN',
  'rootnews': "https://www.mcgill.ca/"
}
iMcGillUniversity = {
    #Good Find Next Find Input
  'id': 'CAN_MCGILLUNI',
  'name': 'Mc Gill University',
  'homelink': 'https://www.mcgill.ca/',
  'newslink': 'https://www.mcgill.ca/newsroom/channels_item/19',  
  'countryid': 'CAN',
  'rootnews': "https://www.mcgill.ca/"
}


iOxfordUniversity = {
    #Good Find Next Find Input
  'id': 'GBR_OXFUN',
  'name': 'University of Oxford',
  'homelink': 'http://www.ox.ac.uk/',
  'newslink': 'https://www.ox.ac.uk/news-listing?category=228',  
  'countryid': 'GBR',
  'rootnews': "https://www.ox.ac.uk/"
}

iCambridgeUniversity = {
  'id': 'GBR_CAMBRIDGEUN',
  'name': 'University of Cambridge',
  'homelink': 'https://www.cam.ac.uk',
  'newslink': 'https://www.cam.ac.uk/research/news',
  'countryid': 'GBR',
  'rootnews': "https://www.cam.ac.uk/"
}

iHarvardUniversity = {
    "id": "USA_MA_HAUN",
    "name": "Harvard University",
    "homelink": "http://harvard.edu",
    "newslink": "https://news.harvard.edu/gazette/section/science-technology/",
    "countryid": "USA",
}


iMassachusettsInstituteofTechnology = {
    "id": "USA_MA_MAINTE",
    "name": "Massachusetts Institute of Technology",
    "homelink": "https://www.mit.edu/",
    "newslink": "https://news.mit.edu/",
    "countryid": "USA",
    "rootnews": "https://news.mit.edu/"
}


iHarvardUniversity = {
    "id": "USA_MA_HAUN",
    "name": "Harvard University",
    "homelink": "http://harvard.edu",
    "newslink": "https://news.harvard.edu/gazette/section/science-technology/",
    "countryid": "USA",
}

iJohnsHopkinsUniversity = {
    "id": "USA_MD_JOHOUN",
    "name": "Johns Hopkins University",
    "homelink": "http://www.jhu.edu/",
    "newslink": "https://hub.jhu.edu/topics/science-technology/articles/",
    "countryid": "USA",
    "rootnews": "https://jobs.jhu.edu"
}
