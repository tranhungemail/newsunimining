# -*- coding: utf-8 -*-
'''
Created on Sep 24, 2017

@author: thg03
'''


from Common.FormatUtil import *
from News.UniNews.UniNews_UniversityInfo import *
from Common.Util import *

from bs4 import BeautifulSoup
import urllib.request
from DataMining.HTMLProcessing import HTMLProcessing
from DataMining.NewsProcessing  import NewsProcessing
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from _datetime import datetime
from urllib3.util import url
from contextlib import suppress

import newspaper
from newspaper import Article


from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import json 

from selenium.webdriver.common.keys import Keys
#from zope.interface.ro import ro
from newspaper.configuration import ArticleConfiguration
from logging import exception



class UniversityofSydney():
    __homelink = iUniversityofSydney['homelink']
    __newslink = iUniversityofSydney['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iUniversityofSydney['homelink']
        __newslink = iUniversityofSydney['newslink']
        
        print('%s Init iUniversityofSydney ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        

        time.sleep(5)

        #oDriver.find_element_by_id("cookie_action_close_header").click()
        #time.sleep(5)
        
        
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"newsItems"}).find_all("a")
        print(rows)
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li['href']
            txtTitle=li.find("h3").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iUniversityofSydney['rootnews'] + tmpLink

            aDeadline=""
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail


class UniversityofNewSouthWales():
    __homelink = iUniversityofNewSouthWales['homelink']
    __newslink = iUniversityofNewSouthWales['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iUniversityofNewSouthWales['homelink']
        __newslink = iUniversityofNewSouthWales['newslink']
        
        print('%s Init iUniversityofNewSouthWales ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        

        time.sleep(5)

        #oDriver.find_element_by_id("cookie_action_close_header").click()
        #time.sleep(5)
        
        
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"view-content"}).find_all("h2")
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            link=li.find("a")
            tmpLink=link['href']
            txtTitle=link.text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iUniversityofNewSouthWales['rootnews'] + tmpLink

            aDeadline=""
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class UniversityofSouthAustralia():
    __homelink = iUniversityofSouthAustralia['homelink']
    __newslink = iUniversityofSouthAustralia['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iUniversityofSouthAustralia['homelink']
        __newslink = iUniversityofSouthAustralia['newslink']
        
        print('%s Init iUniversityofSouthAustralia ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)

        #oDriver.find_element_by_id("cookie_action_close_header").click()
        #time.sleep(5)
        
        
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"stories-card-panel-wrapper"}).find_all("div",{"class":"columns medium-6 large-4"})
        
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            link=li.find("a")
            tmpLink=link['href']
            txtTitle=link.text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iUniversityofSouthAustralia['rootnews'] + tmpLink

            aDeadline=""
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class AustralianNationalUniversity():
    __homelink = iAustralianNationalUniversity['homelink']
    __newslink = iAustralianNationalUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iAustralianNationalUniversity['homelink']
        __newslink = iAustralianNationalUniversity['newslink']
        
        print('%s Init iAustralianNationalUniversity ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)

        #oDriver.find_element_by_id("cookie_action_close_header").click()
        #time.sleep(5)
        
        
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find_all("h2",{"class":"datetext nounderline"})
        
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            link=li.find("a")
            tmpLink=link['href']
            txtTitle=link.text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iAustralianNationalUniversity['rootnews'] + tmpLink

            aDeadline=""
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail


class BondUniversity():
    __homelink = iBondUniversity['homelink']
    __newslink = iBondUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iBondUniversity['homelink']
        __newslink = iBondUniversity['newslink']
        
        print('%s Init iBondUniversity ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)

        #oDriver.find_element_by_id("cookie_action_close_header").click()
        #time.sleep(5)
        
        
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"view-content"}).find_all("h4")
        
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            link=li.find("a")
            tmpLink=link['href']
            txtTitle=link.text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iBondUniversity['rootnews'] + tmpLink

            aDeadline=""
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class NewYorkUniversity():
    __homelink = iNewYorkUniversity['homelink']
    __newslink = iNewYorkUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iNewYorkUniversity['homelink']
        __newslink = iNewYorkUniversity['newslink']
        
        print('%s Init iNewYorkUniversity ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)

        #oDriver.find_element_by_id("cookie_action_close_header").click()
        #time.sleep(5)
        
        
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find_all("div",{"class":"story-info"})
        
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            link=li.find("a")
            tmpLink=link['href']
            txtTitle=link.text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iNewYorkUniversity['rootnews'] + tmpLink

            aDeadline=""
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class EngineeringInstituteofTechnology():
    __homelink = iEngineeringInstituteofTechnology['homelink']
    __newslink = iEngineeringInstituteofTechnology['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iEngineeringInstituteofTechnology['homelink']
        __newslink = iEngineeringInstituteofTechnology['newslink']
        
        print('%s Init iEngineeringInstituteofTechnology ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)

        oDriver.find_element_by_id("cookie_action_close_header").click()
        time.sleep(5)
        
        
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find_all("h4")
        
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            link=li.find("a")
            tmpLink=link['href']
            txtTitle=link.text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iEngineeringInstituteofTechnology['rootnews'] + tmpLink

            aDeadline=""
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class PhenikaaUniversity():
    __homelink = iPhenikaaUniversity['homelink']
    __newslink = iPhenikaaUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iPhenikaaUniversity['homelink']
        __newslink = iPhenikaaUniversity['newslink']
        
        print('%s Init iPhenikaaUniversity ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find_all("div",{"class":"post-title-meta"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            link=li.find("h5").find("a")
            tmpLink=link['href']
            txtTitle=link.text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iPhenikaaUniversity['rootnews'] + tmpLink

            aDeadline=""
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class DuyTanUniversity():
    __homelink = iDuyTanUniversity['homelink']
    __newslink = iDuyTanUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iDuyTanUniversity['homelink']
        __newslink = iDuyTanUniversity['newslink']
        
        print('%s Init iDuyTanUniversity ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"second-news"}).find_all("a")
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li['href']
            txtTitle=li.text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iDuyTanUniversity['rootnews'] + tmpLink

            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail


class VancouverIslandUniversity():
    __homelink = iVancouverIslandUniversity['homelink']
    __newslink = iVancouverIslandUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iVancouverIslandUniversity['homelink']
        __newslink = iVancouverIslandUniversity['newslink']
        
        print('%s Init iVancouverIslandUniversity ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"view-content"}).find_all("div",{"class":"clearfix"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("h4").find("a")['href']
            txtTitle=li.find("h4").find("a").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iVancouverIslandUniversity['rootnews'] + tmpLink

            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail


class ETHZurich():
    __homelink = iETHZurich['homelink']
    __newslink = iETHZurich['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iETHZurich['homelink']
        __newslink = iETHZurich['newslink']
        
        print('%s Init iETHZurich ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("section",{"id":"contentMain"}).find_all("div",{"class":"info"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("a").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iETHZurich['rootnews'] + tmpLink

            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class UniversityofBergen():
    __homelink = iUniversityofBergen['homelink']
    __newslink = iUniversityofBergen['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iUniversityofBergen['homelink']
        __newslink = iUniversityofBergen['newslink']
        
        print('%s Init iUniversityofBergen ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"view-content"}).find_all("div",{"class":"views-field views-field-title"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("a").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iUniversityofBergen['rootnews'] + tmpLink

            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class UniversityofOslo():
    __homelink = iUniversityofOslo['homelink']
    __newslink = iUniversityofOslo['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iUniversityofOslo['homelink']
        __newslink = iUniversityofOslo['newslink']
        
        print('%s Init iUniversityofOslo ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"id":"articleListing.searchComponent"}).find_all("a",{"class":"vrtx-title"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li['href']
            txtTitle=li.text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iUniversityofOslo['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail


class TheUniversityofTokyo():
    __homelink = iTheUniversityofTokyo['homelink']
    __newslink = iTheUniversityofTokyo['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iTheUniversityofTokyo['homelink']
        __newslink = iTheUniversityofTokyo['newslink']
        
        print('%s Init iTheUniversityofTokyo ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"l-row l-gutter-40"}).find_all("div",{"class":"l-col-xs-12 l-col-sm-6"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("p",{"class":"p-news-list__press-releases-item-text"}).text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iTheUniversityofTokyo['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail


class TechnicalUniversityofDenmark():
    __homelink = iTechnicalUniversityofDenmark['homelink']
    __newslink = iTechnicalUniversityofDenmark['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iTechnicalUniversityofDenmark['homelink']
        __newslink = iTechnicalUniversityofDenmark['newslink']
        
        print('%s Init iTechnicalUniversityofDenmark ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"newsList"}).find_all("div",{"class":"newsItem"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("h2").find("a")['href']
            txtTitle=li.find("h2").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iTechnicalUniversityofDenmark['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail


class UniversityofCopenhagen():
    __homelink = iUniversityofCopenhagen['homelink']
    __newslink = iUniversityofCopenhagen['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iUniversityofCopenhagen['homelink']
        __newslink = iUniversityofCopenhagen['newslink']
        
        print('%s Init iUniversityofCopenhagen ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"tiles-y"}).find_all("article")
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("h2").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iUniversityofCopenhagen['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class AaltoUniversity():
    __homelink = iAaltoUniversity['homelink']
    __newslink = iAaltoUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iAaltoUniversity['homelink']
        __newslink = iAaltoUniversity['newslink']
        
        print('%s Init iAaltoUniversity ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        try:
            oDriver.find_element_by_id("cookie-consent-accept").click()
        except:
            pass
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"aalto-rows"}).find_all("h2")
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("a").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iAaltoUniversity['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class HelsikiUniversity():
    __homelink = iHelsikiUniversity['homelink']
    __newslink = iHelsikiUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iHelsikiUniversity['homelink']
        __newslink = iHelsikiUniversity['newslink']
        
        print('%s Init iTheHongKongUniversityofScienceandTechnology ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"l-list"}).find_all("h3")
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("a").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iHelsikiUniversity['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class TheHongKongUniversityofScienceandTechnology():
    __homelink = iTheHongKongUniversityofScienceandTechnology['homelink']
    __newslink = iTheHongKongUniversityofScienceandTechnology['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iTheHongKongUniversityofScienceandTechnology['homelink']
        __newslink = iTheHongKongUniversityofScienceandTechnology['newslink']
        
        print('%s Init iTheHongKongUniversityofScienceandTechnology ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"view-content all-posts"}).find_all("div",{"class":"link"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("a").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iTheHongKongUniversityofScienceandTechnology['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=""
            with suppress(Exception):
                txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class TechnischeUniversitatMunchen():
    __homelink = iTechnischeUniversitatMunchen['homelink']
    __newslink = iTechnischeUniversitatMunchen['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iTechnischeUniversitatMunchen['homelink']
        __newslink = iTechnischeUniversitatMunchen['newslink']
        
        print('%s Init iTechnischeUniversitatMunchen ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("ul",{"id":"news-container-1421"}).find_all("article")
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("a")['title'].strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iTechnischeUniversitatMunchen['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail

class LudwiMaximiliansUniversitatMunchen():
    __homelink = iLudwiMaximiliansUniversitatMunchen['homelink']
    __newslink = iLudwiMaximiliansUniversitatMunchen['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iLudwiMaximiliansUniversitatMunchen['homelink']
        __newslink = iLudwiMaximiliansUniversitatMunchen['newslink']
        
        print('%s Init iLudwiMaximiliansUniversitatMunchen ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find_all("article",{"class":"three-column-img-text__column is-linkbox"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("h5").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iLudwiMaximiliansUniversitatMunchen['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail


class UniversityofWageningen():
    __homelink = iUniversityofWageningen['homelink']
    __newslink = iUniversityofWageningen['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iUniversityofWageningen['homelink']
        __newslink = iUniversityofWageningen['newslink']
        
        print('%s Init iUniversityofWageningen ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=5):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("ul",{"class":"links"}).find_all("li",{"class":"list-item"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            tmpLink=li.find("a")['href']
            txtTitle=li.find("h5").text.strip()
           
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iUniversityofWageningen['rootnews'] + tmpLink

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail
            


class UniversityofMelbourne():
    __homelink = iUniversityofMelbourne['homelink']
    __newslink = iUniversityofMelbourne['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iUniversityofMelbourne['homelink']
        __newslink = iUniversityofMelbourne['newslink']
        
        print('%s Init iUniversityofMelbourne ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=5):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("ul",{"class":"news-block"}).find_all("li",{"class":"news-block__item"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            a=li.find("a")
            txtTitle=li.find("h4").text.strip()
            tmpLink=a['href']
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iUniversityofMelbourne['rootnews'] + li['href']

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail
            


class TheUniversityofBritishColumbia():
    __homelink = iTheUniversityofBritishColumbia['homelink']
    __newslink = iTheUniversityofBritishColumbia['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iTheUniversityofBritishColumbia['homelink']
        __newslink = iTheUniversityofBritishColumbia['newslink']
        
        print('%s Init iTheUniversityofBritishColumbia ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=4):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find_all("a",{"class":"column-link no-scale"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            txtTitle=li.text.strip()
            tmpLink=li['href']
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iTheUniversityofBritishColumbia['rootnews'] + li['href']

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):

        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#open_original_link

            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail
            



class McGillUniversity():
    __homelink = iMcGillUniversity['homelink']
    __newslink = iMcGillUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iMcGillUniversity['homelink']
        __newslink = iMcGillUniversity['newslink']
        
        print('%s Init iMcGillUniversity ok' % str(datetime.now()))

    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"channels-item-list view-more-listing"}).find_all("h2")
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            a=li.find("a")
            txtTitle=a.text.strip()
            tmpLink=a['href']
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iMcGillUniversity['rootnews'] + a['href']

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#open_original_link


            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail
            



class OxfordUniversity():
    __homelink = iOxfordUniversity['homelink']
    __newslink = iOxfordUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iOxfordUniversity['homelink']
        __newslink = iOxfordUniversity['newslink']
        
        print('%s Init iOxfordUniversity ok' % str(datetime.now()))
 
    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"view-content"}).find_all("h2")
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            a=li.find("a")
            txtTitle=a.text.strip()
            tmpLink=a['href']
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iOxfordUniversity['rootnews'] + a['href']

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=1):
        
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#open_original_link

            
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail
            


class CambridgeUniversity():
    __homelink = iCambridgeUniversity['homelink']
    __newslink = iCambridgeUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iCambridgeUniversity['homelink']
        __newslink = iCambridgeUniversity['newslink']
        
        print('%s Init iCambridgeUniversity ok' % str(datetime.now()))
 
    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"view-content"}).find_all("h3",{"class":"cam-teaser-title"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            a=li.find("a")
            txtTitle=a.text.strip()
            tmpLink=a['href']
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iCambridgeUniversity['rootnews'] + a['href']

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#open_original_link

            
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail
            



class MassachusettsInstituteofTechnology():
    __homelink = iMassachusettsInstituteofTechnology['homelink']
    __newslink = iMassachusettsInstituteofTechnology['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iMassachusettsInstituteofTechnology['homelink']
        __newslink = iMassachusettsInstituteofTechnology['newslink']
        
        print('%s Init iMassachusettsInstituteofTechnology ok' % str(datetime.now()))
 
    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"front-page--section-content front-page--recent-news--content"}).find_all("h3",{"class":"front-page--news-article--teaser--title"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            a=li.find("a")
            txtTitle=a.text.strip()
            tmpLink=a['href']
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iMassachusettsInstituteofTechnology['rootnews'] + a['href']

            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        oHTML=HTMLProcessing()
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            
            txtHTML= oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#open_original_link
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail
            


class HarvardUniversity():
    __homelink = iHarvardUniversity['homelink']
    __newslink = iHarvardUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iHarvardUniversity['homelink']
        __newslink = iHarvardUniversity['newslink']
        
        print('%s Init iHarvardUniversity ok' % str(datetime.now()))
 
    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"tz-article-default-list"}).find_all("h2",{"class":"tz-article-image__title"})
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            a=li.find("a")
            txtTitle=a.text.strip()    
            txtJobLink=a['href']
            
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 

    
    
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail
            


class JohnsHopkinsUniversity():
    __homelink = iJohnsHopkinsUniversity['homelink']
    __newslink = iJohnsHopkinsUniversity['newslink']
    
    # chua hieu vi sao. khoi tao gia tri o day, nhung khi goi ham ben duoi thi gia tri k0 con nua.
    def __init__(self):
        __homelink = iJohnsHopkinsUniversity['homelink']
        __newslink = iJohnsHopkinsUniversity['newslink']
        
        print('%s Init iJohnsHopkinsUniversity ok' % str(datetime.now()))
 
    def getTable_NewsLinks(self, num_news=10):
        '''
         return a table with three colums: 
         colums 1: Title of link
         colum 2: Date of Post
         Colum 3: Link of news
        '''

            #for t in tails:
        link=self.__newslink

        oDriver=getChromeDriver()
        oDriver.get(link)
        time.sleep(5)
        page = oDriver.page_source#urllib.request.urlopen(url)
        oDriver.quit()
        soup=BeautifulSoup(page,"html.parser")
        rows = soup.find("div",{"class":"module-content column force"}).find_all("h5")
        final_list=[]
        i=0
        for li in rows:
            intermediate_list = []
            a=li.find("a")
            txtTitle=a.text.strip()    
            tmpLink=a['href']
            if(tmpLink.find("http")!=-1):    
                txtJobLink=tmpLink
            else:
                txtJobLink=iJohnsHopkinsUniversity['rootnews'] + a['href']
            
            aDeadline=""
            
            txtDeadline= getDateFormat(aDeadline)
            intermediate_list.append(txtTitle)#Title of scholarship
            intermediate_list.append(txtDeadline)#Deadline of scholarship
            intermediate_list.append(txtJobLink)#Link of scholarship
            intermediate_list.append(txtJobLink)#tmpJoblink
            i=i+1
            final_list.append(intermediate_list)
            if (i>=num_news):
                break
                
        
                
        return final_list 

    
    
    
    def getContentNews(self,link,open_original_link=0):
        '''
        Input: a news link
        Output: a information of scholarship
        Col 1: HTML of content position
        Col 2: Content of position without HTML. 
        '''       
        
            
        
        try:
            JobDetail=[]
            articleContent = Article(link, language='en', keep_article_html=True)
            articleContent.download()
            articleContent.parse()
            articleContent.nlp()
                
            txtTitle=articleContent.title
            aDateofPost=str(articleContent.publish_date)

            txtDateOfPost=getDateFormat(aDateofPost)    
            

                    
            txtJobLink=articleContent.url
            txtImgLink=articleContent.top_image
            txtSummary=articleContent.summary
            txtKeyword=articleContent.keywords
            oHTML=HTMLProcessing()
            txtHTML=oHTML.clean_HTML4News(articleContent.article_html)
            
            
            txtText=articleContent.text
            
            JobDetail.append(txtTitle)#Title of News
            JobDetail.append(txtDateOfPost)
            JobDetail.append(txtJobLink)            
            JobDetail.append(txtImgLink)#Top Image            
            JobDetail.append(txtSummary)#txtSummary
            JobDetail.append(txtKeyword)#txtKeyword
            JobDetail.append(txtHTML)#txtHTML
            JobDetail.append(txtText)#txtText
            JobDetail.append(open_original_link)#txtText
            
            return JobDetail
        except exception as e:
            print(e.code)
            
            pass
        return JobDetail
            


