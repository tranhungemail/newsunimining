# -*- coding: utf-8 -*-
'''
Created on Sep 24, 2017

@author: thg03
'''


from News.UniNews.UniNews_UniversityInfo import *
from News.UniNews.UniNews_Universities import *


import Common.SysSettings
from DataMining.NewsProcessing import NewsProcessing
from DataAccess.MakeConnect import getConnection
from _datetime import datetime  # waring: fai de imp cu the o dong duoi import *
from selenium.webdriver.common.by import By
from contextlib import suppress

from DataMining import NewsLinks
import uuid
from DataMining.NewsLinks import NewsLink
from langdetect import detect
import re


if __name__ == '__main__':
    pass


'''
    inp: dict of univesity information
    out: the object of university
'''
def getUniversityObject(**university):
    
    
    if university['id'] == iJohnsHopkinsUniversity['id']:
        oGU = JohnsHopkinsUniversity()        
    elif university['id'] == iHarvardUniversity['id']:
        oGU = HarvardUniversity()
    elif university['id'] == iMassachusettsInstituteofTechnology['id']:
        oGU =MassachusettsInstituteofTechnology()
    elif university['id'] == iCambridgeUniversity['id']:
        oGU =CambridgeUniversity()  
    elif university['id'] == iOxfordUniversity['id']:
        oGU =OxfordUniversity()   
    elif university['id'] == iMcGillUniversity['id']:
        oGU =McGillUniversity()  
    elif university['id'] == iTheUniversityofBritishColumbia['id']:
        oGU =TheUniversityofBritishColumbia()  
    elif university['id'] == iUniversityofMelbourne['id']:
        oGU =UniversityofMelbourne()  
    #elif university['id'] == iUniversityofWageningen['id']:
    #    oGU =UniversityofWageningen()  
    elif university['id'] == iLudwiMaximiliansUniversitatMunchen['id']:
        oGU =LudwiMaximiliansUniversitatMunchen()  
    elif university['id'] == iTechnischeUniversitatMunchen['id']:
        oGU =TechnischeUniversitatMunchen()  
    elif university['id'] == iTheHongKongUniversityofScienceandTechnology['id']:
        oGU =TheHongKongUniversityofScienceandTechnology()  
    elif university['id'] == iHelsikiUniversity['id']:
        oGU =HelsikiUniversity()  
    elif university['id'] == iAaltoUniversity['id']:
        oGU =AaltoUniversity() 
    elif university['id'] == iUniversityofCopenhagen['id']:
        oGU =UniversityofCopenhagen() 
    elif university['id'] == iTechnicalUniversityofDenmark['id']:
        oGU =TechnicalUniversityofDenmark()  
    elif university['id'] == iTheUniversityofTokyo['id']:
        oGU =TheUniversityofTokyo()  
    elif university['id'] == iUniversityofOslo['id']:
        oGU =UniversityofOslo()  
    elif university['id'] == iUniversityofBergen['id']:
        oGU =UniversityofBergen()  
    elif university['id'] == iETHZurich['id']:
        oGU =ETHZurich()  
    elif university['id'] == iVancouverIslandUniversity['id']:
        oGU =VancouverIslandUniversity()  
    elif university['id'] == iDuyTanUniversity['id']:
        oGU =DuyTanUniversity()   
    elif university['id'] == iPhenikaaUniversity['id']:
        oGU =PhenikaaUniversity()            
    elif university['id'] == iNewYorkUniversity['id']:
        oGU =NewYorkUniversity()   
    elif university['id'] == iEngineeringInstituteofTechnology['id']:
        oGU =EngineeringInstituteofTechnology()  
    elif university['id'] == iBondUniversity['id']:
        oGU =BondUniversity()          
    elif university['id'] == iAustralianNationalUniversity['id']:
        oGU =AustralianNationalUniversity()    
    elif university['id'] == iUniversityofSouthAustralia['id']:
        oGU =UniversityofSouthAustralia()   
    elif university['id'] == iUniversityofMelbourne['id']:
        oGU =UniversityofMelbourne()  
    elif university['id'] == iUniversityofNewSouthWales['id']:
        oGU =UniversityofNewSouthWales()          
    elif university['id'] == iUniversityofSydney['id']:
        oGU =UniversityofSydney()          
        
    return oGU
           

def main():
    print("==================== UniNews ====================")
    try:
        # create new connection    
        oConnection = getConnection()  
       
        #Australia
        #mining(oConnection, **iUniversityofSydney)
        with suppress(Exception):
            mining(oConnection, **iUniversityofSydney)

        with suppress(Exception):  
            mining(oConnection, **iUniversityofNewSouthWales)
        with suppress(Exception):  
            mining(oConnection, **iUniversityofMelbourne)
        with suppress(Exception):
            mining(oConnection, **iUniversityofSouthAustralia)
        with suppress(Exception):
            mining(oConnection, **iAustralianNationalUniversity)
        with suppress(Exception):
            mining(oConnection, **iBondUniversity)
        with suppress(Exception):
            mining(oConnection, **iEngineeringInstituteofTechnology)
        #End of Australia  
            
        
        with suppress(Exception):
            mining(oConnection, **iNewYorkUniversity)
        
        #exit(1)
        with suppress(Exception):
            mining(oConnection, **iPhenikaaUniversity)
        with suppress(Exception):
            mining(oConnection, **iDuyTanUniversity)
        with suppress(Exception):
            mining(oConnection, **iVancouverIslandUniversity)

        with suppress(Exception):
            mining(oConnection, **iJohnsHopkinsUniversity)
        with suppress(Exception):
            mining(oConnection, **iETHZurich)   
        with suppress(Exception):
            mining(oConnection, **iUniversityofBergen)        
        with suppress(Exception):
            mining(oConnection, **iUniversityofOslo)
        with suppress(Exception):
            mining(oConnection, **iTheUniversityofTokyo)        
        with suppress(Exception):
            mining(oConnection, **iTechnicalUniversityofDenmark)
        with suppress(Exception):
            mining(oConnection, **iUniversityofCopenhagen)
        with suppress(Exception):
            mining(oConnection, **iAaltoUniversity)
        with suppress(Exception): 
            mining(oConnection, **iHelsikiUniversity)
        with suppress(Exception): 
            mining(oConnection, **iTheHongKongUniversityofScienceandTechnology)
        with suppress(Exception): 
            mining(oConnection, **iTechnischeUniversitatMunchen) 
        with suppress(Exception): 
            mining(oConnection, **iLudwiMaximiliansUniversitatMunchen)
        
        with suppress(Exception):        
            mining(oConnection, **iTheUniversityofBritishColumbia)
        with suppress(Exception):
            mining(oConnection, **iMcGillUniversity)
        with suppress(Exception):
            mining(oConnection, **iOxfordUniversity)
        with suppress(Exception):
            mining(oConnection, **iCambridgeUniversity)
        with suppress(Exception):
            mining(oConnection, **iMassachusettsInstituteofTechnology)
        with suppress(Exception):
            mining(oConnection, **iHarvardUniversity)
            
            
        
        
        oConnection._close()
        

    except Exception as e:
        print(">>>>>>>>> Error at function main: University News. Error: " + str(e))

def mining(oConnection, **university):
    # check result of last university
    '''
     This function is to mining data from websites and put them into database
     Steps are as follows:
     1- Scan news from website of universities and organizations
     2- Compare with the existence link in the databases and get the ones which are not in the database.
     3- Put these links into the database and set the flag for these links Status=0 (New)
     4- Crawled data from these links which has Status =0. 
        If its contents is English, 
           Insert this Content into Database tbl_News and set JobStatus=9
           Update tbl_newslinks.Status=2 (News is mined)
        else
           tbl_joblinks.Status=1 (not processed further due to non-english) 
    
    '''
    
    if Common.SysSettings._miningcurrent != "":
        errorat = ">>>>>>>>>>>>>> Mining error at %s <<<<<<<<<<<<<<<<<<" % Common.SysSettings._miningcurrent 
        Common.SysSettings._shortlogcontent = Common.SysSettings._shortlogcontent + '\n' + errorat
        print(errorat)
        
    print("------------- BEGIN ---------------------")
    
    try:        
        print('%s Working at: %s... Country ID: %s' % (str(datetime.now()), university['name'],university['countryid']))
        Common.SysSettings._miningcurrent = university['id']        # set to current
        
        oJobProcessing =  NewsProcessing(oConnection)
        oNewsLinks= NewsLink(oConnection)   
        oGU = getUniversityObject(**university)

        
        ID_Country=university['countryid']
        ID_University=university['id']        
        #aJobLocation=oJobProcessing.getJobLocation(ID_University)
        
        #Get data from the website
        tbl_WebsiteJobLinks=oGU.getTable_NewsLinks() 
        #Get only new links from website        
        lst_New_Links_from_Website=oNewsLinks.getNewNewsLinks(ID_University, tbl_WebsiteJobLinks)
        
                
        NumberPosition=len(lst_New_Links_from_Website)# Real new number of positions
        print('%s There are %s new news in the website.' % (str(datetime.now()), str(NumberPosition)))        
        
        
        countInsertedPositions=0
        
        for i in range(NumberPosition):
            
            print("------"+str(datetime.now())+"-------Mining "+str((i+1))+"/"+str(NumberPosition)+" (total news)----------------------")
            
            aNewsTitle=lst_New_Links_from_Website[i][0]
            aOriginalLink= str(lst_New_Links_from_Website[i][2])# Full Original Link
            #Setting job ID
            u=str(uuid.uuid1())
            ID_NewsLink = ("%s-%s-%s-%s-%s") % (u[24:],u[14:18],u[9:13],u[0:8],u[19:23])
            
            
            if Common.SysSettings._verboselevel > 2:
                    print('%s Mining at link: %s.' % (str(datetime.now()), aOriginalLink))
            
            
            NewsContents=oGU.getContentNews(aOriginalLink) #  Pass link to method to get detail position
            isInserted=False
            aNewsTitle=NewsContents[0]#txtTitle
            aDateofPost=NewsContents[1]#txtDateOfPost
            aOriginalLink=NewsContents[2]#txtJobLink
            txtImgLink=NewsContents[3]#txtImgLink            
            txtSummary=NewsContents[4]#txtSummary
            txtKeyword=', '.join(NewsContents[5])#txtKeyword
            aHTMLContent=NewsContents[6]#txtHTML
            aNonHTMLContent=NewsContents[7]#txtText
            open_original_link=NewsContents[8]#open_original_link

        
            isInserted=oJobProcessing.insertOnePosition_into_tbl_News_RawData(ID_News=ID_NewsLink,NewsTitle=aNewsTitle, DateofPost = aDateofPost,Summary= txtSummary, Keyword=txtKeyword,Top_Image=txtImgLink, NonHTML=aNonHTMLContent, HTML=aHTMLContent, OriginalLink=aOriginalLink, ID_University=ID_University, ID_Country=ID_Country, open_original_link=open_original_link)
            oNewsLinks.insert_One_NewsLink(ID_University=ID_University, ID_Country=ID_Country, ID_NewsLink=ID_NewsLink, Title=aNewsTitle, OriginalLink=aOriginalLink, Status=2)
            
            if isInserted==True:
                countInsertedPositions=countInsertedPositions+1    
        if countInsertedPositions>0: 
            oJobProcessing.updateNumberofLatestNews(university['id'], countInsertedPositions)
        print('- There are %s new news in the website and %s are inserted into database.' % (str(NumberPosition),str(countInsertedPositions)))
        
        # end of mining
        Common.SysSettings._miningcurrent = ""                      # clear done
        Common.SysSettings._miningnewpost += countInsertedPositions     # total news
    except Exception as e:
        print(">>>>>>>> Error at function mining: " + university['name'] + ". Error: " + str(e) + ". Country: "+university['countryid'])
        pass
    
    print("------------- END --------------------------")
    return countInsertedPositions   

    
#MAIN

# Lapbt ---> Luu y khi dung lenh print o cac cho. KHONG print truc tiep, neu k0 qtrong. Neu debug thi lam xong fai closed lai

#print(main())
# Dang sua doi tai day
# END OF MAIN




