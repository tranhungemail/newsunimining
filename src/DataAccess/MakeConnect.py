# -*- coding: utf-8 -*-
'''
Created on Aug 4, 2017

@author: Lapbt
'''
import Common.SysSettings
from DataAccess.PyMySQL import PyMySQL

def getConnection():
    
    # You can change the connection arguments.
    connection = PyMySQL(host=Common.SysSettings._dbHostName, user=Common.SysSettings._dbUserName, password=Common.SysSettings._dbUserPwd, database=Common.SysSettings._dbDatabaseName)
    connection._open()
    return connection


def getConnectionManual(_host, _dbname, _user, _pwd):
    
    # You can change the connection arguments.
    connection = PyMySQL(host=_host, user=_user, password=_pwd, database=_dbname)
    connection._open()
    return connection


def testDAL():
    oCnn = getConnection()
       
    #print(oCnn.insert('test_person',name='Tran Hung', id=15, addr='Ha Noi'))
    
    #print(oCnn.update('test_person', "name = 'Tran Hung' ", name='Alejandro'))
    #print(oCnn.delete('test_person', "name = 'Tran Hung' "))
    
    #print(oCnn.select('test_person', "name like '%Hung%' ", 'id','name'))
    print(oCnn.select('test_person', None,'id','name'))

    #print(oCnn.call_store_procedure('getperson', 'Alejandro'))
    
#print(testDAL())
