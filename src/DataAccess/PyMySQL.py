# -*- coding: utf-8 -*-
'''
Created on Aug 4, 2017

@author: Lapbt

This object is data access layer for access mysql database by pymysql connector

How to use it:

from PyMySQL import PyMySQL
connection = PyMySQL(host='localhost', user='root', password='', database='test')

#Assuming that our table have the fields id and name in this order.
#we can use this way but the parameter should have the same order that table
#connection.insert('table_name',parameters to insert)
connection.insert('test',1, 'Alejandro Mora')
#in this case the order isn't matter
#connection.insert('table_name', field=Value to insert)
connection.insert('test',name='Alejandro Mora', id=1)

#connection.select('Table', where="conditional(optional)", field to returned)
connection.select('test', where="name = 'Alejandro Mora' ")
connection.select('test', None,'id','name')

#connection.update('Table', where="conditional(optional)", field=Value to update)
connection.update('test', None, name='Alejandro')
connection.update('test', where="name = 'Alejandro Mora' ", name='Alejandro')

#connection.delete('Table', where)
connection.delete('test', where="name = 'Alejandro Mora' ")

#connection.call_store_procedure(prodecure name, Values)
connection.call_store_procedure('search_users_by_name', 'Alejandro')

Tham khao them: https://github.com/PyMySQL/PyMySQL

'''
import code
import pymysql.cursors
from _datetime import datetime


class PyMySQL(object):
    __instance = None

    __host = None
    __user = None
    __password = None
    __database = None
    __charset = 'utf8mb4'

    __session = None
    __connection = None

    '''def __new__(cls, *args, **kwargs):
        if not cls.__instance:
            cls.__instance = super(PyMySQL, cls).__new__(cls, *args, **kwargs)
        return cls.__instance
    '''

    def __init__(self, host='localhost', user='root', password='', database=''):
        self.__host = host
        self.__user = user
        self.__password = password
        self.__database = database
        print ('%s Initiation connection to %s successful' % (str(datetime.now()), host))
        

    #Open connection with database
    def _open(self):
        try:
            cnx = pymysql.connect(host=self.__host,
                                 user=self.__user,
                                 password=self.__password,                            
                                 db=self.__database,
                                 charset=self.__charset,
                                 cursorclass=pymysql.cursors.DictCursor)
            self.__connection = cnx
            self.__session = cnx.cursor()
            print ('%s Open connection successful' % str(datetime.now()))
        except pymysql.err.InternalError as err:
            code, msg = err.args
            if code == err.ER_ACCESS_DENIED_ERROR:
                print ('Something is wrong with your user name or password. %s ' % msg)
            elif err.errno == err.ER_BAD_DB_ERROR:
                print ('Database does not exists. %s '  % msg)
            else:
                print (err)
                                

    def _close(self):
        self.__session.close()
        self.__connection.close()
        
       
    def getHostName(self):
        return self.__host
    
    '''
    select data from one table, use syntax
        print(oCnn.select('test_person', "name like '%Hung%' ", 'id','name'))
        
    return all row matched with list data type
    '''
    def select(self, table, where=None, *args):
        result = None
        query = "SELECT "
        keys = args
        l = len(keys) - 1
        for i, key in enumerate(keys):
            query += "`"+key+"`"
            if i < l:
                query += ","
        query += " FROM %s" % table
        ''' can be change to: query =  "SELECT `" + "`,`".join(keys) + "` FROM " + table'''
        
        if where:
            query += " WHERE %s" % where
        #self._open()
        #print(query)
        self.__session.execute(query)
        '''
        Lapbt changed code below for fix error: AttributeError: 'DictCursor' object has no attribute 'stored_results'
        self.__connection.commit()
        for result in self.__session.stored_results():
            result = result.fetchall()
        '''
        result = self.__session.fetchall()
        #self._close()
        return result
    
    
    '''
    select data by your query string, use syntax
        print(oCnn.select("select * from test_person where name like '%Hung%'"))
        
    return all row matched with list data type
    '''   
    def selectSql(self, query):
        #self._open()
        #print(query)
        self.__session.execute(query)
        result = self.__session.fetchall()
        #self._close()
        return result
    
    '''
    select data by your query string and parameters, use syntax
        query = "select * from test_person where name like %s"
        parameter = pa1, pa2, pa3...
        
    return all row matched with list data type
    '''   
    def selectSqlPara(self, query, *parameters):
        #self._open()
        self.__session.execute(query, (parameters))
        result = self.__session.fetchall()
        #self._close()
        return result

    '''
    select data by your query string, use syntax
        print(oCnn.select("select * from test_person where name like '%Hung%'"))
        
    return one row
    '''   
    def selectSqlFetOne(self, query):
        #self._open()
        #print(query)
        self.__session.execute(query)
        result = self.__session.fetchone()
        #self._close()
        return result
    
    
    '''
    select data by your query string and parameters, use syntax
        query = "select * from test_person where name like %s"
        parameter = pa1, pa2, pa3...
        
    return one row
    '''   
    def selectSqlFetOnePara(self, query, *parameters):
        #self._open()
        #print(query)
        self.__session.execute(query, (parameters))
        result = self.__session.fetchone()
        #self._close()
        return result
    
    
    '''
    insert one row to table, the syntax like that: 
        print(oCnn.insert('test_person',name='Tran Hung', id=1, addr='Ha Noi'))
    
    return lastrowid with integer data type
    '''   
    def insert(self, table, *args, **kwargs):
        values = None
        query = "INSERT INTO %s " % table
        if kwargs:
            keys = kwargs.keys()
            values = kwargs.values()
            query += "(" + ",".join(["`%s`"]*len(keys)) % tuple(keys) + ") VALUES(" + ",".join(["%s"]*len(values)) + ")"
        elif args:
            values = args
            query += " VALUES(" + ",".join(["%s"]*len(values)) + ")"
        #self._open()
        '''
        print(query)
        print(values)
        print(list(values))
        
        Warning by Lapbt - 04/08/2017:
            problem: function execute of pymysql connector (may be) only understand values with list data type.
            but this code generate values to dict data type. Therefore, when running will see below error
                    File "C:\Python\Python36\lib\site-packages\pymysql\converters.py", line 73, in _escape_unicode
                    return value.translate(_escape_table)
                    AttributeError: 'dict_values' object has no attribute 'translate'
                    
            solution: must be convert data type from dict to list. See below code 
        '''
        #self.__session.execute(query, values)
        self.__session.execute(query, list(values))
        self.__connection.commit()
        #self._close()
        return self.__session.lastrowid
    
    '''
    update data in one table, use syntax
        print(oCnn.update('test_person', "name = 'Tran Hung' ", name='Alejandro'))
        
    return the number of rows effected with integer data type
    '''  
    def update(self, table, where=None, **kwargs):
        query = "UPDATE %s SET" % table
        keys = kwargs.keys()
        values = kwargs.values()
        l = len(keys) - 1
        for i, key in enumerate(keys):
            query += "`"+key+"`=%s"
            if i < l:
                query += ","
        
        if where:
            query += " WHERE %s" % where
        #self._open()
        #print(query)
        #print(values)
        self.__session.execute(query, list(values))
        self.__connection.commit()
        #self._close()
        return self.__session.rowcount


    '''
    delete data in one table, use syntax
        print(oCnn.delete('test_person', "name = 'Tran Hung' "))
        
    return the number of rows effected with integer data type
    '''  
    def delete(self, table, where):
        query = "DELETE FROM %s WHERE %s" % (table, where)
        #self._open()
        self.__session.execute(query)
        self.__connection.commit()
        #self._close()
        return self.__session.rowcount

    
    '''
    call a procedure, use syntax
        print(oCnn.call_store_procedure('getperson', 'Alejandro'))
        
    return the list of rows, that're return from proc
    '''  
    def call_store_procedure(self, name, *args):
        result_sp = None
        #self._open()
        self.__session.callproc(name, args)
        self.__connection.commit()
        result_sp = self.__session.fetchall()
        #for result in self.__session.stored_results():
        #    result_sp = result.fetchall()
        #self._close()
        return result_sp
    
    
    '''
    to execute your sql string (update, insert, update. NO SELECT), use syntax
        print(oCnn.executeNonQuery("Update from table where 1=2"))
        
    return no data return
    '''  
    def executeNonQuery(self, sql):
        #self._open()
        self.__session.execute(sql)
        self.__connection.commit()
        #self._close()
        
    
    
    '''
    to execute your sql string with parameter (update, insert, update. NO SELECT), use syntax
        print(oCnn.executeNonQuery("Update from table where 1=2"))
        
    return NO RETURN
    '''  
    def executeNonQueryPara(self, sql, *parameters):
        #self._open()
        self.__session.execute(sql, (parameters))
        self.__connection.commit()
        #self._close()
        
    
    
    '''
    to execute your sql string (update, insert, update. NO SELECT), use syntax
        print(oCnn.executeNonQuery("Update from table where 1=2"))
        
    return the number of rows effected, with integer data type
    '''      
    def executeNonQueryAsRowcount(self, sql):
        #self._open()
        self.__session.execute(sql)
        self.__connection.commit()
        #self._close()
        return self.__session.rowcount
    
    
    '''
    to execute your sql string with parameter (update, insert, update. NO SELECT), use syntax
        print(oCnn.executeNonQuery("Update from table where 1=2"))
        
    return the number of rows effected
    '''  
    def executeNonQueryParaAsRowcount(self, sql, *parameters):
        #self._open()
        self.__session.execute(sql, (parameters))
        self.__connection.commit()
        #self._close()
        return self.__session.rowcount
    
    
    
    '''
    to execute your sql string with parameter (update, insert, update. NO SELECT), use syntax
        print(oCnn.executeNonQuery("Update from table where 1=2"))
        
    return lastrowid
    '''  
    def executeNonQueryParaAslastrowid(self, sql, *parameters):
        #self._open()
        self.__session.execute(sql, (parameters))
        self.__connection.commit()
        #self._close()
        return self.__session.lastrowid
    
    
    
